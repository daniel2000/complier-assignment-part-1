//
// Created by attar on 10-May-20.
//

#include "SemanticAnalysis.h"

SemanticAnalysis::SemanticAnalysis(ASTStatementNode * programNode) {
    // Storing the pointer to the program node in a class variable
    this->programNode = programNode;

    // Creating a new instance of the class SemanticAnalysisVisitor and storing it in a class variable
    auto sAVisitor = new SemanticAnalysisVisitor;
    this->semanticAnalysisVisitor = sAVisitor;

}

bool SemanticAnalysis::performSemanticAnalysis() {
    //Visiting the program node
    programNode->accept(semanticAnalysisVisitor);

    // Returning whether a semantic error has been found or not
    return !semanticAnalysisVisitor->semanticError;

}
