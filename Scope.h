//
// Created by attar on 10-May-20.
//

#ifndef COMPLIER_SCOPE_H
#define COMPLIER_SCOPE_H

#include <string>
#include <vector>
#include <unordered_map>
using namespace std;

class Scope {
public:
    // Maps to store bindings

    // String key - identifier, tuple value - string for type, string for value
    unordered_map<string, tuple<string, string>> variableBindings;
    //String key - identifier, tuple value - string for return type, vector of strings for formal params, string for value
    unordered_map<string, tuple<string, vector<string>, string>> functionBindings;
};


#endif //COMPLIER_SCOPE_H
