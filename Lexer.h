#ifndef COMPLIER_LEXER_H
#define COMPLIER_LEXER_H

using namespace std;

#include "NextChar.h"
#include <string>
#include <iostream>
#include <algorithm>
#include <stdio.h>
#include <string.h>
#include <tuple>
#include <stack>

// Defining constants to be used throughout this file
#define STRINGLENGTH 50
#define TYPELENGTH 3
#define NOOFRESERVEDWORDS 17
#define NOOFCHAR 52
#define CATINDEXLETTER 3
#define CATINDEXDIGIT 0
#define CATINDEXUNDERSCORE 4

// Creating an enum to store all of the possible token types
enum TOKEN_TYPE {
    TOK_EOF,
    TOK_SYNTAX_ERROR,
    TOK_BLOCK_COMMENT_START,
    TOK_BLOCK_COMMENT_END,
    TOK_LINE_COMMENT,
    TOK_INTEGER_LITERAL,
    TOK_FLOAT_LITERAL,
    TOK_IDENTIFIER,
    TOK_WHITESPACE,
    TOK_IF,
    TOK_TYPE,
    TOK_AUTO,
    TOK_BOOLEAN_LITERAL,
    TOK_MULTIPLICATIVE_OP,
    TOK_ADDITIVE_OP,
    TOK_RELATIONAL_OP,
    TOK_NOT,
    TOK_LET,
    TOK_PRINT,
    TOK_RETURN,
    TOK_ELSE,
    TOK_FOR,
    TOK_FF,
    TOK_WHILE,
    TOK_ASSIGNMENT_EQUAL,
    TOK_STATEMENT_DELIMITER,
    TOK_OPEN_SCOPE ,
    TOK_CLOSE_SCOPE,
    TOK_ROUND_OPEN_BRACKET,
    TOK_ROUND_CLOSE_BRACKET,
    TOK_COLON,
    TOK_COMMA,
    TOK_EMPTY,
    TOK_NEWLINE,
};

// Creating a struct token which stores the toke type and one of the following,
// lexeme or type or location or valueInt or valueFloat or valueBool
struct token {
    TOKEN_TYPE tokenType;
    char lexeme[STRINGLENGTH];
    union {
        char type[TYPELENGTH];
        char ** location;
    };
};

class Lexer {
private:
    // Transition Table
    int tx[17][13] = {{2,1,-1,5,5,10,7,12,13,14,14,16,-1},
                      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
                      {2,-1,3,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
                      {4,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
                      {4,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
                      {6,-1,-1,6,6,-1,-1,-1,-1,-1,-1,-1,-1},
                      {6,-1,-1,6,6,-1,-1,-1,-1,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,9,8,-1,-1,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,11,-1,-1,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,-1,12,-1,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,-1,-1,13,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,-1,-1,-1,15,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,-1,-1,13,-1,-1,-1,-1},
                      {-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1}};

    // Category Table
    char cat[2][14][NOOFCHAR] = {{{'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'}, {'+', '-'}, {'.'}, {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'}, {'_'}, {'*'}, {'/'}, {' '}, {'\n'}, {'>', '='}, {'<', '>', '='}, {';', '{', '}', '(', ')', ':', '-', ','}, {'\377'}, {"Se"}},
                                 {{'0'},                                              {'1'},      {'2'}, {'3'},                                                                                                                                                                                                                                                                {'4'}, {'5'}, {'6'}, {'7'}, {'8'},  {'9'},      {"10"},          {"11"},                                   {"12"},   {"12"}}};

    // Accepted States
    int acceptedStates[15] = {1,2,4,5,6,7,8,9,10,11,12,13,14,15,16};

    // Token Types
    char type[2][18][NOOFCHAR] = {{{"-1"},           {'0'},       {'1'},          {'2'},   {'3'},       {'4'},     {'5'},          {'6'},          {'7'},                {'8'},           {'9'},                 {"10"},               {"11"},              {"12"},         {"13"},      {"14"},           {"15"},           {"16"}},
                                  {{"reservedWord"}, {"invalid"}, {"additiveOp"}, {"int"}, {"invalid"}, {"float"}, {"identifier"}, {"identifier"}, {"multiplicativeOp"}, {"lineComment"}, {"blockCommentStart"}, {"multiplicativeOp"}, {"blockCommentEnd"}, {"whiteSpace"}, {"newLine"}, {"relationalOp"}, {"relationalOp"}, {"punctuation"}}
    };

    // Reserved Words
    char reservedWords[NOOFRESERVEDWORDS][7] = {{"float"},{"int"},{"bool"},{"auto"},{"true"},{"false"},{"and"},{"or"},{"let"},{"not"},{"print"},{"return"},{"if"},{"else"},{"for"},{"ff"},{"while"}};

    // Stack object
    stack<int> newStack;

    // Function to clear stack
    void clearStack(stack<int> s);

    // String to return after having an invalid entry
    char invalid[STRINGLENGTH]= "Invalid";

    // Obtaining the Token type of the inputted state
    char * TokenType(int state);

    // Function to get the next word in the code
    char * nextWord();

    // Char pointer variable to store the current character which is being used
    char * character  = nullptr;

    // Char double pointer to store the value of the location of the current character pointer
    char ** wrongCharacter;

    // An string to temporarily store the lexeme of each token
    string lexeme = "";

    // Integer array to store if a reserved word is still able to match the word found in the code
    int reservedWordsState[NOOFRESERVEDWORDS] = {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1};

    // Checking if an equal number of opening and closing brackets can be found in the code
    int finalCheckForCorrectNumberOfOpenCloses();

    // Counter used while matching the reserved words
    int charCounter = 0;

    // Function to find the category of a character
    int charcat(char * charPtr);

    // Resetting the array which holds whether a reserved word is still able to be matched or not
    void resetReservedWordState();

    // Copying a string to a char pointer
    void copyString(char * d, string s);

    // An object of type token to store the previous token passed
    token prevToken {TOK_EMPTY};

    // Object of type NextChar used in order to get the next character
    NextChar * nextCharacter;

    // Boolean used to check whether the EOF has been reached
    bool eof = false;

    // A Function to check if the next character is a letter or a digit or an underscore
    bool checkNextCharForLetterDigitUnderScore(char * character);

    // Function to check for reserved words
    bool checkForReservedWord(char * character);

    // Function to check if the number of closed brackets is greater than the number of open brackets
    bool closedBracket(int position);

    // Function to compare a string to a single character
    bool compareStringToCharacter(string str1, char ctr);

    // Checking if the state is an accepted one or not
    bool isApprovedState(int state);

    // Integer array to store the number of opened brackets
    // open[0] = block Comment Start
    // open[1] = round Bracket Start
    // open[2] = open Scope Start
    int open[3] = {0,0,0};

    // Core method of getNextToken()
    token getNextTokenCore();

public:
    // Constructor of class Lexer accepting a pointer to an object of type NextChar
    Lexer(char * firstCodeCharacter);

    // Function to obtain the next token
    token getNextToken();

    // A function to compare a char * with a string
    bool compareCharWithStrings(char * str1, string str2);

    int lineNumber = 1;

    void displayErrorLine(token t);

};


#endif //COMPLIER_LEXER_H
