#include "GenerateXMLFromAST.h"
#include <fstream>
#include <iostream>

// Constructor
void GenerateXMLFromAST::GenerateXML(ASTProgramNode * programNode, string filename){

    // Declaring the outputstream
    ofstream outfile;

    // Opening the file in write node
    outfile.open( "..//" + filename+ ".xml");

    // Creating an instance of the XMLGenerationVisitor with the output file stream
    xmlGenerator = new XMLGenerationVisitor(&outfile);

    // Visiting the inputted node (Program Node)
    xmlGenerator->visit(programNode);

    // Closing the opened file
    outfile.close();

}