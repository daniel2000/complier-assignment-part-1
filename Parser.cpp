#include "Parser.h"
using namespace std;

// Constructor for class Parser, accepting an object of type Lexer
Parser::Parser(char * firstCodeCharacter){
    // Creating an object of type lexer and storing it in the class variable lex
    lex = new Lexer(firstCodeCharacter);
}

ASTProgramNode * Parser::ParseProgram() {
    // Declaring a new Program node
    auto programNode = new ASTProgramNode();
    programNode->name = "Program";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    //Checking for the end of file
    if (currentToken.tokenType == TOK_EOF){
        cout <<" Error/Empty file"<< endl;
        return nullptr;
    }

    // Declaring an empty node
    ASTStatementNode * statementNode  = nullptr;

    do {
        // Parsing a statement
        statementNode = ParseStatement();

        // Checking for null pointers
        if (statementNode == nullptr) {
            return nullptr;
        }

        // Adding the statement to the list of statement in the program node list
        programNode->statementNodeList.push_back(statementNode);

    }
    // The above is computed until the end of file is reached
    while (currentToken.tokenType != TOK_EOF);

    return programNode;
}

ASTStatementNode * Parser::ParseBlock() {
    // Declaring a new ASTBlockNode
    auto blockNode = new ASTBlockNode();

    // Assigning a name to the node
    blockNode->name="Block";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // while the current token is not the '}'
    while (currentToken.tokenType != TOK_CLOSE_SCOPE) {

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

        if(currentToken.tokenType == TOK_RETURN) {
            returnFound = true;
        }

        // Parsing Statement
        auto statementNode = ParseStatement();

        // Checking for null pointer
        if (statementNode == nullptr){
            return nullptr;
        }

        // Storing the pointer of the statement node in the block Node
        blockNode->statementNodeList.push_back(statementNode);
    }

    return blockNode;
}

ASTStatementNode * Parser::ParseStatement() {
    ASTStatementNode * node = nullptr;
    if (currentToken.tokenType == TOK_LET){
        // Variable Declaration found

        // Parsing a variable Declaration
        node =  ParseVariableDeclaration();

        //Checking for null pointers
        if (node == nullptr) {
            return nullptr;
        }

        //Checking for a ';'
        if (currentToken.tokenType != TOK_STATEMENT_DELIMITER) {
            returnErrorMessage(";");
            return nullptr;
        }

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

    } else if (currentToken.tokenType == TOK_IDENTIFIER) {
        // Assignment Statement found
        node = ParseAssignment();

        //Checking for null pointers
        if (node == nullptr) {
            return nullptr;
        }

        //Checking for a ';'
        if (currentToken.tokenType != TOK_STATEMENT_DELIMITER) {
            returnErrorMessage(";");
            return nullptr;
        }

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }


    } else if (currentToken.tokenType == TOK_PRINT) {
        // Print Statement found

        // Parsing print
        node = ParsePrint();

        //Checking for null pointers
        if (node == nullptr) {
            return nullptr;
        }

        //Checking for a ';'
        if (currentToken.tokenType != TOK_STATEMENT_DELIMITER) {
            returnErrorMessage(";");
            return nullptr;
        }

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }


    } else if (currentToken.tokenType == TOK_IF) {
        // If Statement found
        node = ParseIf();

    } else if (currentToken.tokenType == TOK_FOR) {
        // For Statement found
        node = ParseFor();

    } else if (currentToken.tokenType == TOK_WHILE) {
        // While Statement found
        node = ParseWhile();

    } else if (currentToken.tokenType == TOK_RETURN) {
        // Return Statement found
        node = ParseReturn();

        //Checking for null pointers
        if (node == nullptr) {
            return nullptr;
        }

        //Checking for a ';'
        if (currentToken.tokenType != TOK_STATEMENT_DELIMITER) {
            returnErrorMessage(";");
            return nullptr;
        }

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }


    } else if (currentToken.tokenType == TOK_FF) {
        // Function Declaration found
        node = ParseFunctionDecl();

    } else if (currentToken.tokenType == TOK_OPEN_SCOPE) {
        // Block found

        node = ParseBlock();

        //Checking for null pointers
        if (node == nullptr) {
            return nullptr;
        }

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

    } else {
        // Reporting an error message to the user if non of the above reserved words was matched
        returnErrorMessage("let OR identifier OR print OR return OR if OR for OR while OR ff OR {");
        return nullptr;
    }

    return node;
}

ASTExpressionNode * Parser::ParseFormalParam() {
    // Initializing a new ASTFormalParamNode
    auto formalParamNode = new ASTFormalParamNode();
    formalParamNode->name = "FormalParam";

    // Parsing the identifier
    auto identifierNode = ParseIdentifier();

    // Checking for null pointers
    if (identifierNode == nullptr){
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking for a ':'
    if(currentToken.tokenType != TOK_COLON) {
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    //Checking for int or float or bool
    if (currentToken.tokenType == TOK_TYPE) {
        identifierNode->type = currentToken.lexeme;
    } else {
        return nullptr;
    }

    // Storing the pointer to the identifier node in the FormalParam node
    formalParamNode->identifierNode = identifierNode;

    return formalParamNode;
}

ASTExpressionNode * Parser::ParseFormalParams(){
    // Initializing a new ASTFormalParams node
    auto formalParamsNode = new ASTFormalParamsNode();
    formalParamsNode->name = "FormalParams";

    // Parsing the FormalParam
    auto formalParamNode  = ParseFormalParam();

    // Checking for null pointers
    if (formalParamNode == nullptr){
        return nullptr;
    }

    // Storing the pointer of the formalParamNode in the formalParams node
    formalParamsNode->formalParamNodesList.push_back(formalParamNode);

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // while a ',' is present
    while (currentToken.tokenType == TOK_COMMA) {

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

        // Parsing Formal Param
        formalParamNode = ParseFormalParam();

        // Checking for null pointers
        if (formalParamNode == nullptr){
            return nullptr;
        }

        // Storing the pointer of the formalParamNode in the formalParams node
        formalParamsNode->formalParamNodesList.push_back(formalParamNode);

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

    }

    return formalParamsNode;
}

ASTStatementNode * Parser::ParseFunctionDecl(){
    auto functionDeclNode = new ASTFunctionDeclNode();
    functionDeclNode->name = "FunctionDecl";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parsing the identifier
    auto identifierNode = ParseIdentifier();

    // Checking for null pointers
    if(identifierNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the identifierNode in the functionDeclNode
    functionDeclNode->identifierNode = identifierNode;

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking  if the current token is an '('
    if (currentToken.tokenType != TOK_ROUND_OPEN_BRACKET){
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking if the current token is an ')'
    if (currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET) {
        // Parsing FormalParams
        auto formalParamsNode = ParseFormalParams();

        // Checking for null pointers
        if (formalParamsNode == nullptr){
            return nullptr;
        }

        // Storing the pointer to the formalParamsNode in the FunctionDeclNode
        functionDeclNode->formalParamsNode = formalParamsNode;

        // Since after FormalParams, a ')' needs to be found
        if (currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET) {
            return nullptr;
        }
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking for ':"
    if (currentToken.tokenType != TOK_COLON){
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking that the current token is a type/auto
    if (currentToken.tokenType == TOK_TYPE || currentToken.tokenType == TOK_AUTO) {
        functionDeclNode->type = currentToken.lexeme;
    } else {
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parsing a block
    auto blockNode = ParseBlock();

    // Checking for null pointers and that a return nodes exits within the statements
    if (blockNode == nullptr || !returnFound) {
        return nullptr;
    }

    //Storing a pointer to the blockNode in the functionDeclNode
    functionDeclNode->blockNode = blockNode;



    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    return functionDeclNode;
}

ASTStatementNode * Parser::ParseWhile() {
    auto whileNode = new ASTWhileNode();
    whileNode->name = "While";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking if the current token is '('
    if (currentToken.tokenType != TOK_ROUND_OPEN_BRACKET){
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parsing the expression
    auto expressionNode = ParseExpression();

    // Checking for null pointers
    if (expressionNode == nullptr){
        return nullptr;
    }

    // Storing the pointer to the expression node in the While node
    whileNode->expressionNode = expressionNode;

    // Checking if the current token is ')'
    if (currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET){
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parsing a block
    auto blockNode = ParseBlock();

    // Checking for null pointers
    if(blockNode == nullptr){
        return nullptr;
    }

    // Storing the pointer to the block node in the While node
    whileNode->blockNode = blockNode;

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    return whileNode;
}

ASTStatementNode * Parser::ParseFor(){
    auto forNode = new ASTForNode();
    forNode->name = "For";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking that the current token is an open bracket
    if (currentToken.tokenType != TOK_ROUND_OPEN_BRACKET){
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking for a ';'
    if (currentToken.tokenType != TOK_STATEMENT_DELIMITER){
        // Parse Variable Declaration
        auto variableDeclNode = ParseVariableDeclaration();

        // Checking for a null pointers
        if (variableDeclNode == nullptr) {
            return nullptr;
        }

        // Storing the pointer of the variableDeclnode in the for node
        forNode->variableDeclNode = variableDeclNode;

        // Checking for the semi colon
        if (currentToken.tokenType != TOK_STATEMENT_DELIMITER) {
            return nullptr;
        }
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parsing expression
    auto expressionNode = ParseExpression();

    // Checking for null pointers
    if (expressionNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the expression in the for node
    forNode->expressionNode = expressionNode;

    // Checking for ';'
    if(currentToken.tokenType != TOK_STATEMENT_DELIMITER) {
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking for a ')'
    if (currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET){
        // Parse Assignment
        auto assignmentNode = ParseAssignment();

        // Checking if the parsing returned a null pointer
        if(assignmentNode == nullptr) {
            return nullptr;
        }

        // Storing the pointer of the assignment node in the for node
        forNode->assignmentNode = assignmentNode;

        // Checking for the ')'
        if (currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET) {
            return nullptr;
        }

    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parsing the block
    auto blockNode = ParseBlock();

    // Checking if the parsing returned a null pointer
    if (blockNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the block in the for node
    forNode->blockNode = blockNode;

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    return forNode;
}

ASTStatementNode * Parser::ParseIf(){
    // Declaring a new If node
    auto ifNode = new ASTIfNode();
    ifNode->name = "If";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking that the token is an '('
    if(currentToken.tokenType != TOK_ROUND_OPEN_BRACKET) {
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parse the expression
    auto expressionNode = ParseExpression();

    // Checking for nullptr
    if (expressionNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the expression node in the if Node
    ifNode->expressionNode = expressionNode;

    // Checking that the token is an ')'
    if(currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET) {
        returnErrorMessage(")");
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parse the expression
    auto * blockNode = ParseBlock();

    // Checking for nullptr
    if (blockNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the block node in the if Node
    ifNode->trueBlockNode = blockNode;

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking for 'else'
    if (currentToken.tokenType == TOK_ELSE){
        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

        // Parse the expression
        blockNode = ParseBlock();

        // Checking for nullptr
        if (blockNode == nullptr) {
            return nullptr;
        }

        // Storing the pointer of the block node in the if Node
        ifNode->falseBlockNode = blockNode;

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }
    }

    return ifNode;
}

ASTStatementNode * Parser::ParseReturn(){
    // Declaring a new Return node
    auto returnNode = new ASTReturnNode();
    returnNode->name = "Return";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parse the expression
    auto expressionNode = ParseExpression();

    // Checking for nullptr
    if (expressionNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the expression node in the return Node
    returnNode->expressionNode = expressionNode;

    return returnNode;
}

ASTStatementNode * Parser::ParsePrint() {
    // Declaring a new Print node
    auto printNode = new ASTPrintNode();
    printNode->name = "Print";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parse the expression
    auto expressionNode = ParseExpression();

    // Checking for nullptr
    if (expressionNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the expression node in the print Node
    printNode->expressionNode = expressionNode;

    return printNode;
}

ASTStatementNode * Parser::ParseAssignment() {
    // Declaring a new Assignment node
    auto assignmentNode = new ASTAssignmentNode();
    assignmentNode->name = "Assignment";

    // Parsing the Identifier, which the current token is currently on
    auto identifierNode = ParseIdentifier();

    // Checking for nullptr
    if (identifierNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the identifier node in the Assignment Node
    assignmentNode->identifierNode = identifierNode;

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking that the token is an '='
    if(currentToken.tokenType != TOK_ASSIGNMENT_EQUAL) {
        returnErrorMessage("=");
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parse the expression
    auto expressionNode = ParseExpression();

    // Checking for nullptr
    if (expressionNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the expression node in the Assignment Node
    assignmentNode->expressionNode = expressionNode;

    return assignmentNode;
}

ASTStatementNode * Parser::ParseVariableDeclaration() {
    // Declaring a new Variable Declaration node
    auto variableDeclarationNode = new ASTVariableDeclNode();
    variableDeclarationNode->name = "VariableDeclaration";

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    //Parsing an identifier
    auto identifierNode = ParseIdentifier();

    // Checking for null pointers
    if (identifierNode == nullptr) {
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking that the current token is a colon
    if (currentToken.tokenType != TOK_COLON) {
        returnErrorMessage(":");
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking that the current token is a type
    if (currentToken.tokenType != TOK_TYPE && currentToken.tokenType != TOK_AUTO) {
        returnErrorMessage("int or float or bool or auto");
        return nullptr;
    }

    // Storing the type of the identifier
    identifierNode->type = currentToken.lexeme;

    // Storing the pointer of the identifier node in the variableDeclarationNode
    variableDeclarationNode->identifierNode = identifierNode;

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Checking that the current token is the assignment operator
    if (currentToken.tokenType != TOK_ASSIGNMENT_EQUAL) {
        returnErrorMessage("=");
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Parsing an expression
    auto expressionNode = ParseExpression();

    // Checking for null pointers
    if(expressionNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the expression node in the variableDeclarationNode
    variableDeclarationNode->expressionNode = expressionNode;

    return variableDeclarationNode;

}

ASTIdentifierNode * Parser::ParseIdentifier(){
    auto node = new ASTIdentifierNode();
    node->name = "Identifier";

    // Storing the id found in the current tokens lexeme in the identifier node
    node->id = currentToken.lexeme;
    return node;
}

ASTIdentifierNode * Parser::ParseIdentifier(string lexeme){
    auto node = new ASTIdentifierNode();
    node->name = "Identifier";

    // Storing the id of the identifier passed as a parameter
    node->id = lexeme;
    return node;
}

ASTExpressionNode * Parser::ParseExpression() {


    // Parsing a simple expression
    auto simpleExpressionNode = ParseSimpleExpression();

    // Checking for null pointers
    if (simpleExpressionNode == nullptr) {
        return nullptr;
    }

    // Checking if a relation operator exists in the expression and hence a expressionNTnode has to be returned
    if (currentToken.tokenType == TOK_RELATIONAL_OP) {

        auto expressionNTNode = new ASTExpressionNTNode();
        expressionNTNode->name = "Expression";

        // Storing the pointer of the simple expression node in the list of simple expression nodes in the expressionNTnode
        expressionNTNode->simpleExpressionNodesList.push_back(simpleExpressionNode);

        // While the token is a relational operator
        while (currentToken.tokenType == TOK_RELATIONAL_OP) {

            // Storing the relational operator in the list of relational operators
            expressionNTNode->relationalOpList.emplace_back(currentToken.lexeme);

            // Obtaining the next token
            currentToken = lex->getNextToken();

            // Checking for lexical error
            if (currentToken.tokenType == TOK_SYNTAX_ERROR){
                return nullptr;
            }

            // Parsing a simple expression
            auto simpleExpressionNode2 = ParseSimpleExpression();

            // Checking for null pointers
            if (simpleExpressionNode2 == nullptr) {
                return nullptr;
            }

            // Storing the pointer of the simple expression node in the list of simple expression nodes in the expressionNTnode
            expressionNTNode->simpleExpressionNodesList.push_back(simpleExpressionNode2);
        }

        return expressionNTNode;
    } else {
        // If no relational operators are found in the expression the simpleExpressionNode can be returned
        return simpleExpressionNode;
    }

}

ASTExpressionNode * Parser::ParseSimpleExpression() {
    // Parsing a term
    auto termNode = ParseTerm();

    // Checking for null pointers
    if (termNode == nullptr){
        return nullptr;
    }

    // If an additive operator is found than a simple expression node has to be created
    if (currentToken.tokenType == TOK_ADDITIVE_OP){

        // Create simple expression node
        auto simpleExpressionNode = new ASTSimpleExpressionNode();
        simpleExpressionNode->name = "SimpleExpression";

        // Storing the pointer of the term node in the list of term nodes in the simpleExpression node
        simpleExpressionNode->termNodesList.push_back(termNode);

        // While the current token is an additive operator
        while (currentToken.tokenType == TOK_ADDITIVE_OP){
            // Adding the additive op to the list of additive ops
            simpleExpressionNode->additiveOpList.emplace_back(currentToken.lexeme);

            // Obtaining the next token
            currentToken = lex->getNextToken();

            // Checking for lexical error
            if (currentToken.tokenType == TOK_SYNTAX_ERROR){
                return nullptr;
            }

            // Parsing a term
            auto termNode2 = ParseTerm();

            // Checking for null pointers
            if (termNode2 == nullptr){
                return nullptr;
            }

            // Storing the pointer of the term node in the list of term nodes in the simpleExpression node
            simpleExpressionNode->termNodesList.push_back(termNode2);
        }

        return simpleExpressionNode;
    } else {
        // If no additive tokens appear, the term node can be returned
        return termNode;
    }



}

ASTExpressionNode * Parser::ParseTerm() {
    // Parsing a factor
    auto factorNode = ParseFactor();

    // Checking for null pointers
    if (factorNode == nullptr){
        return nullptr;
    }


    // If a multiplicative operator is found, than a term node has to be generated
    if (currentToken.tokenType == TOK_MULTIPLICATIVE_OP) {
        auto termNode = new ASTTermNode();
        termNode->name = "Term";

        // Storing the pointer of the factor node in the list of factor nodes in the term node
        termNode->factorNodesList.push_back(factorNode);

        // While the token is a multiplicative operator
        while (currentToken.tokenType == TOK_MULTIPLICATIVE_OP){
            // Adding the multiplicative op to the list of multiplicative ops
            termNode->multiplicativeOpList.emplace_back(currentToken.lexeme);

            // Obtaining the next token
            currentToken = lex->getNextToken();

            // Checking for lexical error
            if (currentToken.tokenType == TOK_SYNTAX_ERROR){
                return nullptr;
            }

            // Parsing a factor
            auto factorNode2 = ParseFactor();

            // Checking for null pointers
            if (factorNode2 == nullptr){
                return nullptr;
            }

            // Storing the pointer of the factor node in the list of factor nodes in the term node
            termNode->factorNodesList.push_back(factorNode2);
        }

        return termNode;
    } else {
        // If a multiplicative operator is not found, a the factor node alone can be returned
        return factorNode;
    }



}

ASTExpressionNode * Parser::ParseFactor() {

    if (currentToken.tokenType == TOK_BOOLEAN_LITERAL || currentToken.tokenType == TOK_INTEGER_LITERAL || currentToken.tokenType == TOK_FLOAT_LITERAL) {
        // Parsing a literal
        auto literalNode = ParseLiteral();

        // Checking for null pointers
        if (literalNode == nullptr){
            return nullptr;
        }

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

        return literalNode;
    } else if (currentToken.tokenType == TOK_IDENTIFIER) {
        // Either an identifier or a Function call was found

        // Temporarily storing the name of the identifier
        string tempLexeme = currentToken.lexeme;

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

        // Checking if the next token is a '('
        if (currentToken.tokenType == TOK_ROUND_OPEN_BRACKET) {
            // A Function call was found

            // Parsing a function call
            auto functionCallNode = ParseFunctionCall(tempLexeme);

            // Checking for null pointers
            if (functionCallNode == nullptr) {
                return nullptr;
            }

            return functionCallNode;

        } else {
            // Else an identifier was found
            // Parsing an identifer with the id that was temporarily stored
            auto identifierNode = ParseIdentifier(tempLexeme);

            return identifierNode;
        }
    } else if (currentToken.tokenType == TOK_ROUND_OPEN_BRACKET) {
        // Sub Expression Found

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

        // Parsing expression
        auto expNode = ParseExpression();

        // Checking for null pointers
        if (expNode == nullptr) {
            return nullptr;
        }

        /*
        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }
         */

        // Checking for a '('
        if(currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET) {
            returnErrorMessage(")");
            return nullptr;
        } else {
            // Obtaining the next token
            currentToken = lex->getNextToken();

            // Checking for lexical error
            if (currentToken.tokenType == TOK_SYNTAX_ERROR){
                return nullptr;
            }

            return expNode;
        }

    } else if ((currentToken.tokenType == TOK_ADDITIVE_OP && currentToken.lexeme[0] == '-') || currentToken.tokenType == TOK_NOT) {
        // Unary found

        auto unaryNode = new ASTUnaryNode();
        unaryNode->name = "Unary";
        //Storing the opposite character (- or not) in the unary node
        unaryNode->oppositeOperator =  currentToken.lexeme;

        // Obtaining the next token
        currentToken = lex->getNextToken();

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

        // Parsing an expression
        auto expNode = ParseExpression();

        // Checking for null pointers
        if (expNode == nullptr) {
            return nullptr;
        }

        // Storing the pointer of the expression node in the unary node
        unaryNode->expressionNode = expNode;

        return unaryNode;
    } else {
        // Returning an error message to the user
        returnErrorMessage("literal OR identifier OR Function Call OR SubExpression OR unary");
        return nullptr;
    }
}

ASTExpressionNode * Parser::ParseFunctionCall(string identifierId) {
    // Since we already matched both the identifier and the '(' the ActualParams is next or ')'
    auto functionCallNode = new ASTFunctionCallNode();
    functionCallNode->name = "FunctionCall";

    // Parsing the identifier matched above
    auto identifierNode = ParseIdentifier(identifierId);

    //Checking for null pointers
    if (identifierNode == nullptr) {
        return nullptr;
    }

    // Storing the pointer of the identifier node in the function call node
    functionCallNode->identifierNode = identifierNode;

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    // Since ActualParams can be excluded, the next token is checked if is it a starting token of an expression (hence a starting token of a Factor)
    if(CheckIfFactor()) {
        // Parsing ActualParams
        auto actualParamsNode = ParseActualParams();

        // Checking for null pointers
        if(actualParamsNode == nullptr) {
            return nullptr;
        }

        // Storing the pointer of the actuaLParamsnode in the functionCallNode
        functionCallNode->actualParamsNode = actualParamsNode;

        //Checking for ')'
        if (currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET) {
            return nullptr;
        }


    } else if (currentToken.tokenType == TOK_ROUND_CLOSE_BRACKET) {
        // Function call successfully matched
    } else {
        return nullptr;
    }

    // Obtaining the next token
    currentToken = lex->getNextToken();

    // Checking for lexical error
    if (currentToken.tokenType == TOK_SYNTAX_ERROR){
        return nullptr;
    }

    return functionCallNode;

}

ASTExpressionNode * Parser::ParseActualParams() {
    auto actualParamsNode = new ASTActualParamsNode();
    actualParamsNode->name = "ActualParams";

    int counter = 0;

    // While the current token in not the close brackets
    while(currentToken.tokenType != TOK_ROUND_CLOSE_BRACKET) {

        if (counter != 0) {
            //Checking if a comma exists when having multiple expression
            if (currentToken.tokenType != TOK_COMMA) {
                returnErrorMessage(",");
                return nullptr;
            } else {
                // Obtaining the next token
                currentToken = lex->getNextToken();

                // Checking for lexical error
                if (currentToken.tokenType == TOK_SYNTAX_ERROR){
                    return nullptr;
                }
            }
        }

        //Parsing the expression
        auto expressionNode = ParseExpression();

        // Checking for null pointers
        if (expressionNode == nullptr) {
            return nullptr;
        }

        // Storing the pointer of the expression node in the actualParamsNode
        actualParamsNode->expressionNodeList.push_back(expressionNode);

        // Checking for lexical error
        if (currentToken.tokenType == TOK_SYNTAX_ERROR){
            return nullptr;
        }

        counter++;
    }


    return actualParamsNode;

}

ASTExpressionNode * Parser::ParseLiteral(){
    auto node = new ASTLiteralNode();
    node->name = "Literal";

    // Choosing which literal and storing the appropriate parameters
    switch(currentToken.tokenType) {
        case TOK_BOOLEAN_LITERAL:
            node->type = "bool";
            node->value = currentToken.lexeme;
            break;
        case TOK_INTEGER_LITERAL:
            node->type = "int";
            node->value = currentToken.lexeme;
            break;
        case TOK_FLOAT_LITERAL:
            node->type = "float";
            node->value = currentToken.lexeme;
            break;
        default:
            // Returning an error message if none of the above literal types match
            returnErrorMessage("bool or int or float");
            return nullptr;
    }

    return node;
}

bool Parser::CheckIfFactor() {
    // Checking if the current token could be a factor token
    if (currentToken.tokenType == TOK_BOOLEAN_LITERAL || currentToken.tokenType == TOK_INTEGER_LITERAL || currentToken.tokenType == TOK_FLOAT_LITERAL) {
        return true;
    } else if (currentToken.tokenType == TOK_IDENTIFIER) {
        return true;
    } else if (currentToken.tokenType == TOK_ROUND_OPEN_BRACKET) {
        return true;
    } else if (currentToken.tokenType == TOK_ADDITIVE_OP && currentToken.lexeme[0] == '-') {
        return true;
    } else if (currentToken.tokenType == TOK_NOT) {
        return true;
    } else {
        return false;
    }
}

void Parser::returnErrorMessage(string expected) {
    // Displaying an error message displaying the line number and lexeme of the current token and what the program expected
    cout << "Parsing Error on line: " << lex->lineNumber <<" near: " << currentToken.lexeme << endl;
    cout << "Expected: " << expected << endl;
}
