//
// Created by attar on 08-May-20.
//

#ifndef COMPLIER_GENERATEXMLFROMAST_H
#define COMPLIER_GENERATEXMLFROMAST_H

#include "Visitor/XMLGenerationVisitor.h"

class GenerateXMLFromAST {
public:
    // To store an instance of the XMLGenerationVisitorClass
    XMLGenerationVisitor * xmlGenerator;
    // Constructor
    void GenerateXML(ASTProgramNode * programNode, string filename);
};


#endif //COMPLIER_GENERATEXMLFROMAST_H
