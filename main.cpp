#include <iostream>
#include "NextChar.h"
#include "Lexer.h"
#include <fstream>
#include <string.h>
#include "Parser.h"
#include "Visitor/XMLGenerationVisitor.h"
#include "GenerateXMLFromAST.h"
#include "SemanticAnalysis.h"
#include "InterpreterExecution.h"

#define maxNoOfCharactersInFile 1000


using namespace std;
//https://deniskyashif.com/2019/02/17/implementing-a-regular-expression-engine/
//https://www.tutorialspoint.com/cplusplus/cpp_files_streams.htm
//http://www.cplusplus.com/reference/cstdio/fgetc/?kw=fgetc


int main() {

    // char array to store all the words from the file
    char code[maxNoOfCharactersInFile]= "";

    // Counter for each character in the file
    int counter = 0;

    // Initializing a file variable
    FILE * pFile;
    char c;
    //Opening the file
    pFile=fopen ("..//tests//test2.txt","r");


    // Checking for error while opening the file
    if (pFile==NULL){
        perror ("Error opening file");
    }
    else
    {
        // Getting every character from the file
        do {
            c = fgetc (pFile);
            code[counter] = c;
            counter++;
        } while (c != EOF );
        // Closing the file
        fclose (pFile);

    }

    // Pointer of the char array storing the code
    char * firstCharPointer = code;

    Parser p1(firstCharPointer);
    ASTProgramNode * n1 = p1.ParseProgram();

    /*
    if (n1 != nullptr) {
        auto xmlGEn = new XMLGenerationVisitor();
        xmlGEn->visit(n1);
    }
     */

    if (n1 != nullptr) {
        GenerateXMLFromAST generateXmlFromAst;
        generateXmlFromAst.GenerateXML(n1,"test");

        SemanticAnalysis sa(n1);
        bool semanticAnalysisReturn = sa.performSemanticAnalysis();

        if (semanticAnalysisReturn) {
            cout<< "Program semantically correct" << endl;

            InterpreterExecution interpreterExecution(n1);
            interpreterExecution.performInterpreterExecution();
        }
    }




/*


    //---------------------------------
    //---------------------------------
    //---------------------------------
    //USE DELETE-
    //---------------------------------
    //---------------------------------
    //---------------------------------
    //---------------------------------



    Lexer l1(firstCharPointer);
    token t1{};


    while(true) {
        t1 = l1.getNextToken();
        cout << "TOKEN TYPE "<< t1.tokenType << endl;
        cout << "TYPE " << t1.type << endl;
        cout << "LEXEME " << t1.lexeme << endl;
        if(t1.tokenType == TOK_EOF) {
            cout<<"END OF FILE"<< endl;
            break;
        } else if (t1.tokenType == TOK_SYNTAX_ERROR) {
            cout << "Error in code near -->" << *t1.location<<"<--" << endl;
            break;
        } else if (t1.tokenType == TOK_IDENTIFIER) {
            cout << "IDENTIFER NAME IS " << t1.lexeme << endl;
        }  else if (t1.tokenType == TOK_IF) {
            cout << "THE IF HAS BEEN FOUND " << t1.lexeme << endl;
        } else if (t1.tokenType == TOK_MULTIPLICATIVE_OP) {
            cout << "THE MULTIPLICATIVEOP HAS BEEN FOUND " << t1.lexeme << endl;
        }
    }

    /*
    int ans = l1.finalCheckForCorrectNumberOfOpenCloses();
    if (ans != -1 && t1.tokenType == TOK_EOF) {
        string temp = "";

        if (ans == 0) {
            temp = "block comments";
        } else if (ans == 1) {
            temp= "round brackets";

        } else if (ans == 2) {
            temp = "curly brackets";

        }
        cout<<"UNEQUAL NO OF"<< temp<<endl;
    }
     */


   // cout << result << endl;
    return 0;
}
