#ifndef COMPLIER_NEXTCHAR_H
#define COMPLIER_NEXTCHAR_H


// Class called NextChar
class NextChar {
private:
    // Counter to store the number of characters already read
    int counter = 0;
    // Char pointer
    char * initialChar;
public:
    // Constructor of the class nextchar
    NextChar(char * initialCharPointer);

    // Function to obtain the next character
    char * NextCharacter(char * charPointer);

    // Function to rollback the character pointer
    char * RollBack(char *charPointer);

};


#endif //COMPLIER_NEXTCHAR_H
