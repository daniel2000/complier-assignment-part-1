using namespace std;


#include "Lexer.h"

// Function to clear stack
void Lexer::clearStack(stack<int> s) {
    while(!s.empty()) {
        s.pop();
    }
}

// Function to find the category of the passed character
int Lexer::charcat(char * charPtr){

    // Calculating the number of "columns"
    int noOfColumns = sizeof(cat[0])/ sizeof(cat[0][0]);

    // Going through each column
    for(int i = 0;i<noOfColumns;i++){
        // Going through each character in each column
        for(int j = 0;j< sizeof(cat[0][i]);j++){

            // Checking if the characters match
            if(cat[0][i][j] == *charPtr){

                // Converting to int the category number
                return  atoi(cat[1][i]);
            }
        }
    }
    // If not found return Se value
    return atoi(cat[1][noOfColumns-1]);
}

// Function to get the next word in the code
char* Lexer::nextWord() {
    // Initialisation
    int state = 0;
    lexeme = "";
    clearStack(newStack);
    // -2 bad state
    // -1 Se empty states
    newStack.push(-2);

    // Scanning
    // While the state is not the empty state
    while(state != -1) {
        // Obtaining the next character from the code
        character = nextCharacter->NextCharacter(character);

        // Adding this character to a string
        lexeme += *character;

        // Checking if the end of file has been reached
        if (*character =='\377') {
           eof = true;
        }

        // Checking if the current state is one of the approved states
        if (isApprovedState(state)) {
            // Clearing the stack
            clearStack(newStack);
        }

        // Adding the current state to the stack
        newStack.push(state);

        // Obtaining the category of the current character
        int category = charcat(character);

        // Checking if the current current is a letter
        if (category == CATINDEXLETTER){
            // If the character is a letter, it has the potential to be a reserved word,
            // this is checked for using another function
            if (checkForReservedWord(character)) {
                // If a reserved word is found, we can proceed to the next step
                return TokenType(-1);
            }
        }

        // Obtaining the new state based on the current state and the category of the new character
        state = tx[state][category];

    }

    // Setting a double pointer to the value of the location of the current character pointer
    // This is used whenever an error is detected, to show the user where the error was found
    wrongCharacter = &character;

    //int rollbackCounter = 0;

    // RollBack Loop

    // While the state is not an approved one or the bad state
    while(!isApprovedState(state) && state != -2) {
        // The last state on the stack is retrieved and removed from the stack
        state = newStack.top();
        newStack.pop();

        // Reduce 1 character from the lexeme each time a new state is removed from the stack
        lexeme[lexeme.size()-1] = '\0';

        // Roll back the character pointer for every state popped from the stack
        // This is since each state represented a character which wont be used in the current word
        character = nextCharacter->RollBack(character);
        //rollbackCounter++;
    }

    /*
    if (rollbackCounter != 1){
        for(int i =0;i<rollbackCounter;i++){
            //character = nextCharacter->RollBack(character);
        }
    }
    */

    // Report Result
    // If the current state is an approved one, its type is returned
    if(isApprovedState(state)){
        return TokenType(state);
    } else {
        // Invalid is returned if the state is not an approved one
        return invalid;
    }


}

token Lexer::getNextToken() {
    token t1{};
    int openBlockComments = 0;
    do {
        t1 = getNextTokenCore();
        // For handling comments
        //If A line comment is detected
        if (t1.tokenType == TOK_LINE_COMMENT){
            // The next token is obtained until a new line token is found or the end of the file has been reached
            while (t1.tokenType != TOK_NEWLINE && t1.tokenType != TOK_EOF){
                t1 = getNextTokenCore();
            }
        } else if (t1.tokenType == TOK_BLOCK_COMMENT_START){
            // A new block comment start has been found
            openBlockComments++;

            // To cover for nested block comments
            // While the end of file is not found
            while (t1.tokenType != TOK_EOF){
                // The next token is obtained
                t1 = getNextTokenCore();

                // If another start block comment is found
                if (t1.tokenType == TOK_BLOCK_COMMENT_START){
                    // Incrementing a counter
                    openBlockComments++;
                } else if (t1.tokenType == TOK_BLOCK_COMMENT_END){
                    // If the end of a block comment was found
                    //Decrementing a counter
                    openBlockComments--;

                    // If the number of open block comments is 0
                    if (openBlockComments == 0){
                        t1 = getNextTokenCore();
                        break;
                    }
                }
            }
        } else if (t1.tokenType == TOK_SYNTAX_ERROR){
            cout << "Lexical Error found on line "<<lineNumber<<" near ";
            displayErrorLine(t1);
        } else if (t1.tokenType == TOK_NEWLINE){
            // Incrementing the number of lines in the program
            lineNumber+= atoi(t1.lexeme);
        }
    }while(t1.tokenType==TOK_WHITESPACE || t1.tokenType == TOK_NEWLINE);

    return t1;
}

// Obtaining the next token
token Lexer::getNextTokenCore() {
    // Obtaining the next work in the code
    char * retNextWord = nextWord();

    //Resetting the required variables for detecting reserved words.
    resetReservedWordState();


    //cout<<retNextWord<<endl;

    // Initializing an invalid token
    token InvalidToken{};
    InvalidToken.tokenType = TOK_SYNTAX_ERROR;
    InvalidToken.location = wrongCharacter;

    // Declaring an empty token to be initialized up and returned
    token t1{};


    if (compareCharWithStrings(retNextWord, "Invalid") && eof) {
        // The end of the file has been reached
        t1.tokenType = TOK_EOF;

        // Storing the lexeme in the token
        copyString(t1.lexeme,lexeme);
    } else if (compareCharWithStrings(retNextWord, "Invalid")) {
        // An error was detected with the code
        return InvalidToken;
    } else if(compareCharWithStrings(retNextWord, "blockCommentStart")) {
        // A block comment start '/*' was found
        t1.tokenType = TOK_BLOCK_COMMENT_START;

        // Incrementing the number of block comment starts found
        open[0]++;

        // Storing the lexeme in the token
        copyString(t1.lexeme,lexeme);
    } else if(compareCharWithStrings(retNextWord, "blockCommentEnd")) {
        // A block comment end '*/' was found
        t1.tokenType = TOK_BLOCK_COMMENT_END;

        // Storing the lexeme in the token
        copyString(t1.lexeme,lexeme);

        // Checking if there is an equal number of block comments starts and ends
        if (!closedBracket(0)) {
            // If there isn'currentFunctionCall returning the invalid token
            return InvalidToken;
        }
    }  else if(compareCharWithStrings(retNextWord, "int")) {
        // An integer literal was found '102'
        t1.tokenType = TOK_INTEGER_LITERAL;

        // Storing the integer value in the lexeme
        copyString(t1.lexeme,lexeme);
    } else if(compareCharWithStrings(retNextWord, "float") ) {
        // A float literal was found '102.5004'
        t1.tokenType = TOK_FLOAT_LITERAL;

        // Storing the float value in the lexeme
        copyString(t1.lexeme,lexeme);
    } else if(compareCharWithStrings(retNextWord, "identifier") ) {
        // An identifier was found 'counter' or 'ConvertStringToInt'
        t1.tokenType = TOK_IDENTIFIER;

        // Storing the name of the identifier in the token
        copyString(t1.lexeme,lexeme);
    } else if(compareCharWithStrings(retNextWord, "lineComment")) {
        // A line comment has been found
        t1.tokenType = TOK_LINE_COMMENT;

        // Storing the lexeme in the token
        copyString(t1.lexeme,lexeme);
    } else if(compareCharWithStrings(retNextWord, "whiteSpace") ) {
        // A white space has been found
        t1.tokenType = TOK_WHITESPACE;

        // Storing the lexeme in the token
        copyString(t1.lexeme,lexeme);
    } else if((compareCharWithStrings(retNextWord, "reservedWord") && lexeme.compare("and") == 0) || compareCharWithStrings(retNextWord, "multiplicativeOp")) {
        // A multiplicative operator was found either the reserved word 'and' or *, /
        t1.tokenType = TOK_MULTIPLICATIVE_OP;

        // Storing which multiplicative operator was found in the token
        copyString(t1.lexeme,lexeme);
    } else if((compareCharWithStrings(retNextWord, "reservedWord") && lexeme.compare("or") == 0) || compareCharWithStrings(retNextWord, "additiveOp")) {
        // An additive operator was found either the reserved word 'or' or +, -
        t1.tokenType = TOK_ADDITIVE_OP;

        // Storing which multiplicative operator was found in the token
        copyString(t1.lexeme,lexeme);
    } else if(compareCharWithStrings(retNextWord, "reservedWord")) {
        if (lexeme.compare("float") == 0 || lexeme.compare("int") == 0 || lexeme.compare("bool") == 0) {
            // A reserved word from the variables types was found
            t1.tokenType = TOK_TYPE;

            // Storing the variable type in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "auto") {
            // The reserved word 'auto' was found
            t1.tokenType = TOK_AUTO;

            // Storing the variable type in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "true" || lexeme == "false") {
            // A boolean literal was found
            t1.tokenType = TOK_BOOLEAN_LITERAL;

            // Storing the boolean value in the lexeme
            copyString(t1.lexeme,lexeme);

        } else if(lexeme == "not") {
            // The reserved word 'not' was found
            t1.tokenType = TOK_NOT;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "let") {
            // The reserved word 'let' was found
            t1.tokenType = TOK_LET;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "print") {
            // The reserved word 'print' was found
            t1.tokenType = TOK_PRINT;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "return") {
            // The reserved word 'return' was found
            t1.tokenType = TOK_RETURN;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "if") {
            // The reserved word 'if' was found
            t1.tokenType = TOK_IF;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "else") {
            // The reserved word 'let' was found
            t1.tokenType = TOK_ELSE;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "for") {
            // The reserved word 'for' was found
            t1.tokenType = TOK_FOR;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "ff") {
            // The reserved word 'ff' was found
            t1.tokenType = TOK_FF;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if(lexeme == "while") {
            // The reserved word 'while' was found
            t1.tokenType = TOK_WHILE;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        }
    } else if(compareCharWithStrings(retNextWord, "relationalOp") && compareStringToCharacter(lexeme,'=')) {
        // An assignment equals has been found
        t1.tokenType = TOK_ASSIGNMENT_EQUAL;

        // Storing the lexeme in the token
        copyString(t1.lexeme,lexeme);
    } else if(compareCharWithStrings(retNextWord, "relationalOp")) {
        // A relational operator has been found
        t1.tokenType = TOK_RELATIONAL_OP;

        // Storing the lexeme in the token
        copyString(t1.lexeme,lexeme);

        if (lexeme == "=="){
            // An equal operator was found
            copyString(t1.type,"EQ");
        } else if (compareStringToCharacter(lexeme,'<')) {
            // A less than operator was found
            copyString(t1.type,"LT");
        } else if (lexeme.compare("<=") == 0) {
            // A less than or equal to operator was found
            copyString(t1.type,"LE");
        } else if (lexeme.compare("<>") == 0) {
            // A not equal to operator was found
            copyString(t1.type,"NE");
        } else if (compareStringToCharacter(lexeme,'>')) {
            // A greater than operator was found
            copyString(t1.type,"GT");
        } else if (lexeme.compare(">=") == 0) {
            // A greater than or equal to operator was found
            copyString(t1.type,"GE");
        }
    } else if(compareCharWithStrings(retNextWord, "punctuation")) {
        // Punctuation was found in the code
        if (compareStringToCharacter(lexeme,';')){
            // A statement delimiter was found
            t1.tokenType = TOK_STATEMENT_DELIMITER;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if (compareStringToCharacter(lexeme, '{' )) {
            // An opening curly bracket was found
            t1.tokenType = TOK_OPEN_SCOPE;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);

            // Incrementing the number of open curly brackets found
            open[2]++;
        } else if (compareStringToCharacter(lexeme,'}')) {
            // A closing curly bracket was found
            t1.tokenType = TOK_CLOSE_SCOPE;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);

            // Checking if the number of opening curly brackets matches up with the number of closing curly brackets
            if (!closedBracket(2)) {
                // If it doesn'currentFunctionCall match up return the invalid token
                return InvalidToken;
            }
        } else if (compareStringToCharacter(lexeme,'(')) {
            // An opening round bracket was found
            t1.tokenType = TOK_ROUND_OPEN_BRACKET;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);

            // Incrementing the number of open round brackets found
            open[1]++;
        } else if (compareStringToCharacter(lexeme, ')')) {
            // An closing round bracket was found
            t1.tokenType = TOK_ROUND_CLOSE_BRACKET;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);

            // Checking if the number of opening round brackets matches up with the number of closing round brackets
            if (!closedBracket(1)) {
                // If it doesn'currentFunctionCall match up return the invalid token
                return InvalidToken;
            }
        } else if (compareStringToCharacter(lexeme, ':')) {
            // A colon was found
            t1.tokenType = TOK_COLON;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        } else if (compareStringToCharacter(lexeme, ',')) {
            // A comma was found
            t1.tokenType = TOK_COMMA;

            // Storing the lexeme in the token
            copyString(t1.lexeme,lexeme);
        }
    } else if(compareCharWithStrings(retNextWord, "newLine")) {
        // A new line was found
        t1.tokenType = TOK_NEWLINE;

        //Storing the valueInt the number of new lines found, in order to correctly display where an error is chosen,
        //The DFA accepts multiple new lines at once and hence without this, the new line counter is inaccurate when multiple new lines come after each other
        string temp = to_string(lexeme.size()/2);
        copyString(t1.lexeme,temp.c_str());
    } else {
        return InvalidToken;
    }
    /*

    // Checking that this is not the first token to be returned
    if (prevToken.tokenType != TOK_EMPTY) {
        // Checking if the previous token was a punctuation, not considering the close round bracket since this can have other punctuation after it
        if (prevToken.tokenType == TOK_STATEMENT_DELIMITER || prevToken.tokenType == TOK_OPEN_SCOPE ||
            prevToken.tokenType == TOK_CLOSE_SCOPE ||prevToken.tokenType == TOK_ROUND_OPEN_BRACKET ||
            prevToken.tokenType == TOK_COLON ||
            prevToken.tokenType == TOK_DASH ||prevToken.tokenType == TOK_COMMA){

            // If double, opening or closing, round or curly, brackets are found, this is allowed
            if ((prevToken.tokenType == TOK_OPEN_SCOPE && t1.tokenType == TOK_OPEN_SCOPE)||
                (prevToken.tokenType == TOK_CLOSE_SCOPE && t1.tokenType == TOK_CLOSE_SCOPE)||
                    (prevToken.tokenType == TOK_ROUND_OPEN_BRACKET && t1.tokenType == TOK_ROUND_OPEN_BRACKET)) {
                //This is allowed

            } else if (compareCharWithStrings(retNextWord,"punctuation") ) {
                // If punctuation is followed by another punctuation, apart from when the first one is a round ending bracket
                // This is an error and the invalid token is returned
                t1.tokenType = TOK_SYNTAX_ERROR;
                t1.location = wrongCharacter;
            }
        }


    }

    // Assigning a object of type token to the values of the token to be returned
    // This is to check what was returned in the previous iteration of getNextToken()
    prevToken.tokenType = t1.tokenType;
    copyString(prevToken.lexeme,t1.lexeme);
    copyString(prevToken.type,t1.type);
    prevToken.location = t1.location;
    prevToken.valueInt = t1.valueInt;
    prevToken.valueFloat = t1.valueFloat;
    prevToken.valueBool = t1.valueBool;
        */
    // Returning the token
    return t1;
}

// Function to check if the number of closed brackets is greater than the number of open brackets
bool Lexer::closedBracket(int position) {
    // Decrementing the counter for that particular position
    open[position]--;

    // Checking if the number of closed brackets is greater than the number of open brackets
    // Checking if the number of open brackets is greater than the number of closed brackets
    // is done at the end after all the code has been gone through
    if (open[position] < 0) {
        return false;
    }

    return true;

}

// Checking if the state is an accepted one or not
bool Lexer::isApprovedState(int state) {
    // Checking if the state is found in the array of accepted states using the function find()
    return find(begin(acceptedStates), end(acceptedStates), state) != end(acceptedStates);
}

// Obtaining the Token type of the inputted state
char * Lexer::TokenType(int state){
    // Calculating the no of columns found in the array
    int noOfColumns = sizeof(type[0])/ sizeof(type[0][0]);

    // For each column i.e. for each different state
    for(int i =0;i<noOfColumns;i++ ) {
        // Finding the state in the array which is equal to the inputted state
        if(atoi(type[0][i]) == state){
            // Returning the type that that state represents
            return type[1][i];
        }
    }
    // If the state is not found, the invalid type is returned
    return invalid ;
}

// Copying a string to a char pointer
void Lexer::copyString(char * destination, string source){
    // Checking that the string can be placed in the char array, keeping in mind to allow space for the \0
    if (source.size() > STRINGLENGTH-1){
        // If the string doesn'currentFunctionCall fit in the char array, an error is returned
        cout<<"ERROR WHEN COPYING STRING"<<endl;
    } else{
        // Copying the string into the char * and finishing with \0
        copy( source.begin(),source.end(),destination);
        destination[source.size()+1] = '\0';
    }
}

// Function to check for reserved words
bool Lexer::checkForReservedWord(char * c) {

    // Going through each reserved word
    for(int i= 0;i< NOOFRESERVEDWORDS;i++) {
        // Checking that the reserved word matched up till now
        if (reservedWordsState[i] == 1) {
            // Checking if the current character matches for both words
            if (*c == reservedWords[i][charCounter]) {
                // If all of the current reserved word was matched, and the next character is not a letter or digit or underscore
                // (i.e. the word is not an identifier very similar to one of the reserved words)
                if ((strlen(reservedWords[i])) == charCounter+1 &&  !checkNextCharForLetterDigitUnderScore(c)){
                    return true;

                }
            } else {
                // If the current character is not matched than the word can be marked as not a possible reserved word
                // This is done by having another array the same length of the number of reserved words
                // This is initially initialized to all 1's and each word which is not a possible match its corresponding value is changed to 0
                reservedWordsState[i] = 0;
            }
        }

    }
    // Incrementing the charCounter
    charCounter++;
    return false;

}

// A Function to check if the next character is a letter or a digit or an underscore
bool Lexer::checkNextCharForLetterDigitUnderScore(char * c) {
    // Obtaining the next character
    c = c + sizeof(char);
    // Getting the category of the character
    int charType = charcat(c);

    // Checking if the next character is a letter of a digit or an underscore
    return charType == CATINDEXLETTER || charType == CATINDEXDIGIT || charType == CATINDEXUNDERSCORE;

}

// A function to reset variables related to finding the reserved words
void Lexer::resetReservedWordState(){
    // Initializing the character counter to 0
    charCounter =0;

    // Initializing all the array to 1's
    for(int i= 0;i< sizeof(reservedWordsState)/ sizeof(reservedWordsState[0]);i++) {
        reservedWordsState[i] = 1;
    }
}

// A function to compare a char * with a string
bool Lexer::compareCharWithStrings(char * str1, string str2) {
    int counter = 0;
    // While the character from the char array is the same as the character from the string
    while (*str1 == str2[counter]){
        // If the null character is found, break out of the loop
        if(*str1 == '\0' || str2[counter] == '\0'){
            break;
        }

        // Incrementing the counters
        str1++;
        counter++;
    }

    // If both the char array and the string and at the end, then the two match
    return *str1 == '\0' && str2[counter] == '\0';
}

// Constructor for class Lexer
Lexer::Lexer(char * firstCodeCharacter) {
    // The constructor accepts a object of type NextChar which is initialized to the class public variable of type NextChar
    nextCharacter = new NextChar(firstCodeCharacter);
}

// Checking if an equal number of opening and closing brackets can be found in the code
int Lexer::finalCheckForCorrectNumberOfOpenCloses() {
    // Going through the array which is incremented once an opening bracket is found and decremented once a closing bracket is found
    for(int i=0;i< sizeof(open)/ sizeof(open[0]);i++){
        // If the value is not 0, there is an imbalance
        if (open[i] != 0) {
            return i;
        }
    }

    return -1;
}

// Function to compare a string to a single character
bool Lexer::compareStringToCharacter(string str1, char ctr) {
    // Checking that the first character of the string is the same as the character and that the second character of the string is null
    return str1[0] == ctr && str1[1] == '\0';
}

void Lexer::displayErrorLine(token t) {
    token t1{};

    // Storing a double pointer to the current location where the error occurred
    char ** temp  = t.location;
    // Storing a pointer to the current character
    char * tempChar = *temp;

    do {
        // Displaying the character
        cout<<*tempChar;

        // Obtaining the next character until the new line charcter is found
        tempChar = nextCharacter->NextCharacter(tempChar);
    }while (*tempChar != '\n');

    // Skip a line
    cout<<""<<endl;

}