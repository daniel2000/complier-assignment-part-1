//
// Created by attar on 12-May-20.
//

#include "InterpreterExecution.h"

InterpreterExecution::InterpreterExecution(ASTProgramNode * programNode){
    this->programNode = programNode;

    auto interpreterExecutionVisitor = new InterpreterExecutionVisitor();
    this->interpreterExecutionVisitor = interpreterExecutionVisitor;
}

void InterpreterExecution::performInterpreterExecution() {
    // Visit the program node
    this->programNode->accept(this->interpreterExecutionVisitor);
}