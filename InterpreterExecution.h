//
// Created by attar on 12-May-20.
//

#ifndef COMPLIER_INTERPRETEREXECUTION_H
#define COMPLIER_INTERPRETEREXECUTION_H

#include "SymbolTable.h"
#include "Visitor//InterpreterExecutionVisitor.h"

class InterpreterExecution {
public:
    SymbolTable * symbolTable;

    ASTProgramNode * programNode;

    InterpreterExecutionVisitor * interpreterExecutionVisitor;

    InterpreterExecution(ASTProgramNode * programNode);

    void performInterpreterExecution();

};


#endif //COMPLIER_INTERPRETEREXECUTION_H
