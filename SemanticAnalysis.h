//
// Created by attar on 10-May-20.
//

#ifndef COMPLIER_SEMANTICANALYSIS_H
#define COMPLIER_SEMANTICANALYSIS_H
#include "ASTNodes//StatementNodes/ASTStatementNode.h"
#include "Visitor//SemanticAnalysisVisitor.h"

class SemanticAnalysis {
public:
    // Pointer to the program node
    ASTStatementNode * programNode;

    // Pointer to an instance of the semantic analysis visitor class
    SemanticAnalysisVisitor * semanticAnalysisVisitor;

    // Function to start sementic analysis on the program node
    bool performSemanticAnalysis();

    // Constructor
    SemanticAnalysis(ASTStatementNode * programNode);
};


#endif //COMPLIER_SEMANTICANALYSIS_H
