//
// Created by attar on 21-Apr-20.
//
using namespace std;
#include "NextChar.h"
#include <iostream>

// Function to obtain the next character
char * NextChar::NextCharacter(char *charPointer) {
    // If this is the first character to be read
    if (counter == 0) {
        // The counter is incremented
        counter++;

        // The pointer to the initialCharacter of the code is returned
        return initialChar;
    } else {
        // The counter is incremented
        counter++;

        // The size of a character is added to the given pointer and this is returned
        return charPointer + sizeof(char);
    }

}

// Function to rollback the character pointer
char * NextChar::RollBack(char *charPointer){
    // Decreasing the number of characters read
    counter--;

    // If the number of characters read is below 0, we are trying to read beyond the input
    if (counter < 0){
        cout << "Rolled back beyond input" << endl;
        return nullptr;
    } else {
        // Decreasing the current pointer by the size of char
        return charPointer - sizeof(char);
    }

}

// Constructor of the class nextchar, which accepts a pointer to the code array
NextChar::NextChar(char * initialCharPointer){
    initialChar = initialCharPointer;

}