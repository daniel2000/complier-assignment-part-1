//
// Created by attar on 09-May-20.
//

#ifndef COMPLIER_SYMBOLTABLE_H
#define COMPLIER_SYMBOLTABLE_H

#include <stack>
#include <unordered_map>
#include <string>
#include "Scope.h"
#include <tuple>

using namespace std;

class SymbolTable {
public:
    // Stack of pointers to objects of type scope
    stack<Scope *> scopes;

    // Add a new scope to the stack
    void push();

    // Remove the last scope from the stack
    void pop();

    // Insert a variable binding with id and type
    void insertVariable(string id, string type);

    // Insert a function binding with id and type
    void insertFunction(string id, string type);

    // Search for variable binding
    tuple<string,string , int> lookupVariable(string id);

    // Search for function binding
    // Returns Tuple with return type, the value and the scope number where it was found
    tuple<string,string, int> lookupFunction(string id);

    // Add parameters to a function declaration
    void addParameterToFunction(string functionId, string parameterType);

    // Change the return type of a function
    void changeFunctionType(string functionId, string returnType);

    // Update the value of a variable
    void updateVariableValue(string variableId, string value);

    // Update the value of a function
    void updateFunctionValue(string functionId, string value);
};


#endif //COMPLIER_SYMBOLTABLE_H
