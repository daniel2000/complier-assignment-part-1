//
// Created by attar on 09-May-20.
//

#include "SymbolTable.h"

// Add a new scope to the stack
void SymbolTable::push() {
    // Creating an object of type scope and pushing it on the stack
    auto s = new Scope();
    scopes.push(s);
}

// Remove the last scope from the stack
void SymbolTable::pop() {
    // 'Deleting' the last instance of scope, found on the top of the stack
    scopes.pop();
}

// Insert a variable binding with id and type
void SymbolTable::insertVariable(string id, string type) {
    // Obtaining the pointer to the current scope
    auto currentScope = scopes.top();

    // Adding a binding to the current scope, with no value
    currentScope->variableBindings.insert({id,make_tuple(type,"")});
}

// Insert a function binding with id and type
void SymbolTable::insertFunction(string id, string type) {
    // Obtaining the pointer to the current scope
    auto currentScope = scopes.top();

    // Tuple made up of string and empty vector of string for the formal parameters and empty string for the value
    vector<string> temp;
    auto tuple = make_tuple(type,temp,"");

    // Adding a binding to the current scope
    currentScope->functionBindings.insert({id,tuple});
}

// Search for variable binding
tuple<string, string ,int> SymbolTable::lookupVariable(string id) {
    // Declaring a copy of the stack of scopes
    stack<Scope *> tempStack = scopes;

    tuple<string,string> lookupReturn;
    while (!tempStack.empty()) {
        // Getting the top scope
        auto currentScope = tempStack.top();
        try {
            // Searching for a binding with key id
            lookupReturn = currentScope->variableBindings.at(id);
            // if the key is found return a tuple with the identifier type, its current value and the number of the scope it was found on
            return make_tuple(get<0>(lookupReturn),get<1>(lookupReturn), tempStack.size());
        } catch (out_of_range) {
            // Identifier not found in current scope
            // Removing the scope just searched
            tempStack.pop();
        }
    }

    // If the id was not found in any scope
    return make_tuple("notFound","",tempStack.size());
}

// Search for function binding
tuple<string,string, int> SymbolTable::lookupFunction(string id) {
    // Declaring a copy of the stack of scopes
    stack<Scope *> tempStack = scopes;

    tuple<string,vector<string>,string> bindingFound;
    while (!tempStack.empty()) {
        // Getting the top scope
        auto currentScope = tempStack.top();
        try {
            // Searching for a binding with key id
            bindingFound = currentScope->functionBindings.at(id);
            // if the key is found return a tuple with the identifier type the current value of the function and the number of the scope it was found on
            return make_tuple(get<0>(bindingFound), get<2>(bindingFound),tempStack.size());
        } catch (out_of_range) {
            // Identifier not found in current scope
            // Removing the scope just searched
            tempStack.pop();

        }
    }

    // If the id was not found in any scope
    return make_tuple("notFound","",tempStack.size());
}

// Add parameters to a function declaration
void SymbolTable::addParameterToFunction(string functionId, string parameterType){
    // Declaring a copy of the stack of scopes
    stack<Scope *> tempScopesStack = scopes;

    // A new scope was added so that the parameter variables can be used within the fuction
    // The function binding is actually done in the previous scope, therefore we have to go to the previous scope
    tempScopesStack.pop();

    // Getting the current value for the functionId (key)
    auto currentfunctionValue = tempScopesStack.top()->functionBindings.at(functionId);

    // Getting the vector of parameter types
    vector<string> tempParameterTypes = get<1>(currentfunctionValue);

    // Adding the new parameter type to the vector of parameter types
    tempParameterTypes.push_back(parameterType);

    // Storing the new value of the key
    tempScopesStack.top()->functionBindings[functionId] = make_tuple(get<0>(currentfunctionValue),tempParameterTypes,get<2>(tempScopesStack.top()->functionBindings[functionId]));
}

// Change the return type of a function
void SymbolTable::changeFunctionType(string functionId, string returnType) {

    // Searching for the current function to find in which scope the binding is found
    tuple<string, string ,int> lookUpReturn = lookupFunction(functionId);

    // Declaring a copy of the stack of scopes
    stack<Scope *> tempScopesStack = scopes;

    // Getting the scope the binding was found in
    while(tempScopesStack.size() != get<2>(lookUpReturn)) {
        tempScopesStack.pop();
    }

    // Obtaining the current value of the key
    auto currentFunctionValue = tempScopesStack.top()->functionBindings.at(functionId);

    // Storing the new value of the key
    tempScopesStack.top()->functionBindings[functionId] = make_tuple(returnType,get<1>(currentFunctionValue),get<2>(currentFunctionValue));

}

void SymbolTable::updateVariableValue(string variableId, string value) {

    // Searching for the current variable to find in which scope the binding is found
    tuple<string, string ,int> lookUpReturn = lookupVariable(variableId);

    // Declaring a copy of the stack of scopes
    stack<Scope *> tempScopesStack = scopes;

    // Getting the scope the binding was found in
    while(tempScopesStack.size() != get<2>(lookUpReturn)) {
        tempScopesStack.pop();
    }

    // Obtaining the current value of the key
    auto currentVariableValue = tempScopesStack.top()->variableBindings.at(variableId);

    // Storing the new value of the key
    tempScopesStack.top()->variableBindings[variableId] = make_tuple(get<0>(currentVariableValue),value);
}

void SymbolTable::updateFunctionValue(string functionId, string value) {
    // Searching for the current function to find in which scope the binding is found
    tuple<string, string ,int> lookUpReturn = lookupFunction(functionId);

    // Declaring a copy of the stack of scopes
    stack<Scope *> tempScopesStack = scopes;

    // Getting the scope the binding was found in
    while(tempScopesStack.size() != get<2>(lookUpReturn)) {
        tempScopesStack.pop();
    }

    // Getting the current value for the functionId (key)
    auto currentfunctionMapValue = tempScopesStack.top()->functionBindings.at(functionId);

    // Storing the new value of the key
    tempScopesStack.top()->functionBindings[functionId] = make_tuple(get<0>(currentfunctionMapValue),get<1>(currentfunctionMapValue),value);

}