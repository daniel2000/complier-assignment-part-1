#ifndef COMPLIER_PARSER_H
#define COMPLIER_PARSER_H

#include "Lexer.h"
#include <stack>
#include "Visitor/XMLGenerationVisitor.h"

class Parser {
private:
    // To store an instance of type Lexer
    Lexer * lex;

    // Variable to store the current token
    token currentToken;

    // Functions to parse different statements
    ASTStatementNode * ParseStatement();
    ASTStatementNode * ParseBlock();
    ASTStatementNode * ParsePrint();
    ASTStatementNode * ParseReturn();
    ASTStatementNode * ParseIf();
    ASTStatementNode * ParseFor();
    ASTStatementNode * ParseWhile();
    ASTStatementNode * ParseAssignment();
    ASTStatementNode * ParseFunctionDecl();
    ASTStatementNode * ParseVariableDeclaration();

    // Functions to parse different expressions
    ASTExpressionNode * ParseExpression();
    ASTExpressionNode * ParseSimpleExpression();
    ASTExpressionNode * ParseTerm();
    ASTExpressionNode * ParseFactor();
    ASTExpressionNode * ParseLiteral();
    ASTExpressionNode * ParseActualParams();
    ASTExpressionNode * ParseFormalParam();
    ASTExpressionNode * ParseFormalParams();
    ASTExpressionNode * ParseFunctionCall(string identifierId);

    ASTIdentifierNode * ParseIdentifier();
    ASTIdentifierNode * ParseIdentifier(string lexeme);


    //Returns and error message to the user
    void returnErrorMessage(string expected);

    // Checks if the current node is one which can be parsed as a factor
    bool CheckIfFactor();

    // Checks in a return statement has been found
    bool returnFound;

public:
    // Constructor
    Parser(char * firstCodeCharacter);

    // Function to parse the Program
    ASTProgramNode * ParseProgram();
};


#endif //COMPLIER_PARSER_H
