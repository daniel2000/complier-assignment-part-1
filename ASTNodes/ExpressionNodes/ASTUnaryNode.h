//
// Created by attar on 08-May-20.
//

#ifndef COMPLIER_ASTUNARYNODE_H
#define COMPLIER_ASTUNARYNODE_H

#include "ASTExpressionNode.h"

// Inherits ASTExpressionNode
class ASTUnaryNode: public ASTExpressionNode {
public:
    // Pointer to expression node
    ASTExpressionNode * expressionNode;

    // storing the opposite operator
    string oppositeOperator;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTUNARYNODE_H
