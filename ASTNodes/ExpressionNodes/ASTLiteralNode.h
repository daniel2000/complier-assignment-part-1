//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTLITERALNODE_H
#define COMPLIER_ASTLITERALNODE_H

#include "ASTExpressionNode.h"
#include <string>
using namespace std;

// Inherits ASTExpressionNode
class ASTLiteralNode : public ASTExpressionNode{
public:
    // TYpe of literal (bool or int or float)
    string type;

    // Value of literal
    string value;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTLITERALNODE_H
