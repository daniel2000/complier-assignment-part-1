#ifndef COMPLIER_ASTACTUALPARAMSNODE_H
#define COMPLIER_ASTACTUALPARAMSNODE_H
#include "ASTExpressionNode.h"

// Inherits ASTExpressionNode
class ASTActualParamsNode : public ASTExpressionNode{
public:
    // List of pointer to expression nodes
    vector<ASTExpressionNode *> expressionNodeList;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTACTUALPARAMSNODE_H
