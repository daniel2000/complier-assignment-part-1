//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTFUNCTIONCALLNODE_H
#define COMPLIER_ASTFUNCTIONCALLNODE_H

#include "ASTExpressionNode.h"

// Inherits ASTExpressionNode
class ASTFunctionCallNode : public ASTExpressionNode{
public:
    //Pointer to identifier node
    ASTExpressionNode * identifierNode;
    //Pointer to OPTIONAL  actualParams node
    ASTExpressionNode * actualParamsNode = nullptr;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTFUNCTIONCALLNODE_H
