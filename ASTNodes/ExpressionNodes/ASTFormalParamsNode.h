//
// Created by attar on 08-May-20.
//

#ifndef COMPLIER_ASTFORMALPARAMSNODE_H
#define COMPLIER_ASTFORMALPARAMSNODE_H
#include "ASTExpressionNode.h"

// Inherits ASTExpressionNode
class ASTFormalParamsNode : public ASTExpressionNode{
public:
    //List of pointers to formal Param Nodes
    vector<ASTExpressionNode *> formalParamNodesList;


    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }

};


#endif //COMPLIER_ASTFORMALPARAMSNODE_H
