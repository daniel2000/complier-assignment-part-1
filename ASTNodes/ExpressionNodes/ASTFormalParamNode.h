//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTFORMALPARAMNODE_H
#define COMPLIER_ASTFORMALPARAMNODE_H

#include "ASTExpressionNode.h"

// Inherits ASTExpressionNode
class ASTFormalParamNode : public ASTExpressionNode{
public:
    // Pointer to identifier node
    ASTExpressionNode * identifierNode;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }


};


#endif //COMPLIER_ASTFORMALPARAMNODE_H
