//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTTERMNODE_H
#define COMPLIER_ASTTERMNODE_H

#include "ASTExpressionNode.h"
#include <vector>
using namespace std;

class ASTTermNode : public ASTExpressionNode {
public:
    // List of pointers to factor nodes
    vector<ASTExpressionNode *> factorNodesList;

    // List of strings of multiplicative operators
    vector<string> multiplicativeOpList;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTTERMNODE_H
