//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTEXPRESSIONNTNODE_H
#define COMPLIER_ASTEXPRESSIONNTNODE_H

#include "ASTExpressionNode.h"

// Inherits ASTExpressionNode
class ASTExpressionNTNode : public ASTExpressionNode {
public:
    //List of pointers to simple expression nodes
    vector<ASTExpressionNode *> simpleExpressionNodesList;
    // List of strings of relational operators
    vector<string> relationalOpList;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }

};


#endif //COMPLIER_ASTEXPRESSIONNTNODE_H
