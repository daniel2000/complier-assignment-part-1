//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTEXPRESSIONNODE_H
#define COMPLIER_ASTEXPRESSIONNODE_H


#include <vector>
#include <string>
#include "../../Visitor/Visitor.h"

using namespace std;

class ASTExpressionNode {
public:
    // Variable to store name of node 
    string name;
    //Visitor handle
    virtual void accept(Visitor* v) = 0;

};


#endif //COMPLIER_ASTEXPRESSIONNODE_H
