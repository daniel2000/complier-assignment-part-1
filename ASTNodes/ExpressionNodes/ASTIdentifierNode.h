//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTIDENTIFIERNODE_H
#define COMPLIER_ASTIDENTIFIERNODE_H

#include "ASTExpressionNode.h"
#include <string>
using namespace std;

// Inherits ASTExpressionNode
class ASTIdentifierNode: public ASTExpressionNode {
public:
    // String to store id (name) of identifier
    string id;

    // String to store type of identifier
    string type = "";

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }

};


#endif //COMPLIER_ASTIDENTIFIERNODE_H
