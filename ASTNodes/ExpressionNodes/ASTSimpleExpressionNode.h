//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTSIMPLEEXPRESSIONNODE_H
#define COMPLIER_ASTSIMPLEEXPRESSIONNODE_H

#include "ASTExpressionNode.h"


// Inherits ASTExpressionNode
class ASTSimpleExpressionNode : public ASTExpressionNode{
public:
    // List of pointers to term nodes
    vector<ASTExpressionNode *> termNodesList;
    
    //List of strings of additive operators
    vector<string> additiveOpList;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTSIMPLEEXPRESSIONNODE_H
