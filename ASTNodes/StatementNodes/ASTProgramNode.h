//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTPROGRAMNODE_H
#define COMPLIER_ASTPROGRAMNODE_H
#include "ASTStatementNode.h"

//Inherits ASTStatementNode
class ASTProgramNode : public ASTStatementNode {
public:
    // List of pointers to statement nodes
    vector<ASTStatementNode *> statementNodeList;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }

};


#endif //COMPLIER_ASTPROGRAMNODE_H
