//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTIFNODE_H
#define COMPLIER_ASTIFNODE_H

#include "ASTStatementNode.h"
#include "../ExpressionNodes/ASTExpressionNode.h"

// Inherits ASTStatementNode
class ASTIfNode : public ASTStatementNode {
public:
    // Pointer to expression Node
    ASTExpressionNode * expressionNode = nullptr;

    // Pointer to block node
    ASTStatementNode * trueBlockNode;

    // Pointer to block node
    ASTStatementNode * falseBlockNode = nullptr;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }

};


#endif //COMPLIER_ASTIFNODE_H
