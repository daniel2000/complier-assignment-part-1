//
// Created by attar on 08-May-20.
//

#ifndef COMPLIER_ASTFUNCTIONDECLNODE_H
#define COMPLIER_ASTFUNCTIONDECLNODE_H

#include "ASTStatementNode.h"
#include "..//ExpressionNodes/ASTExpressionNode.h"

// Inherits ASTStatementNode
class ASTFunctionDeclNode : public ASTStatementNode{
public:
    // Pointer to identifier node
    ASTExpressionNode * identifierNode;

    // Pointer to formal params node
    ASTExpressionNode * formalParamsNode = nullptr;

    // Return type of function
    string type;

    // Pointer to block node
    ASTStatementNode * blockNode;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }


};


#endif //COMPLIER_ASTFUNCTIONDECLNODE_H
