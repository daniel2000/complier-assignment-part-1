//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTFORNODE_H
#define COMPLIER_ASTFORNODE_H

#include "ASTStatementNode.h"
#include "../ExpressionNodes/ASTExpressionNode.h"

// Inherits ASTStatementNode
class ASTForNode : public ASTStatementNode {
public:
    // Pointer to variableDeclNode
    ASTStatementNode * variableDeclNode = nullptr;

    // Pointer to expressionNode
    ASTExpressionNode * expressionNode = nullptr;

    // Pointer to assignmentNode
    ASTStatementNode * assignmentNode = nullptr;

    // Pointer to blockNode
    ASTStatementNode * blockNode = nullptr;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTFORNODE_H
