//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTASSIGNMENTNODE_H
#define COMPLIER_ASTASSIGNMENTNODE_H

#include "ASTStatementNode.h"
#include "../ExpressionNodes/ASTExpressionNode.h"

// Inherits ASTStatementNode
class ASTAssignmentNode: public ASTStatementNode {
public:
    // Pointer to identifier node
    ASTExpressionNode * identifierNode;
    // Pointer to expression node
    ASTExpressionNode * expressionNode;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }

};


#endif //COMPLIER_ASTASSIGNMENTNODE_H
