//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTRETURNNODE_H
#define COMPLIER_ASTRETURNNODE_H

#include "ASTStatementNode.h"
#include "../ExpressionNodes/ASTExpressionNode.h"

// Inherits ASTStatementNode
class ASTReturnNode: public ASTStatementNode {
public:
    // Pointer to expression node
    ASTExpressionNode * expressionNode;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }

};


#endif //COMPLIER_ASTRETURNNODE_H
