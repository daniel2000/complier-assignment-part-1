//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTPRINTNODE_H
#define COMPLIER_ASTPRINTNODE_H

#include "ASTStatementNode.h"
#include "../ExpressionNodes/ASTExpressionNode.h"

// Inherits ASTStatementNode
class ASTPrintNode: public ASTStatementNode {
public:
    // Pointer to expression node
    ASTExpressionNode * expressionNode;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTPRINTNODE_H
