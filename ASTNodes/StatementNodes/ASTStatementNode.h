//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTSTATEMENTNODE_H
#define COMPLIER_ASTSTATEMENTNODE_H

#define LIMIT 100

using namespace std;

#include <vector>
#include <memory>
#include <iostream>
#include "../../Visitor/Visitor.h"

class ASTStatementNode{
public:
    // String to store name of node
    string name;
    //Visitor handle
    virtual void accept(Visitor* v) = 0;
};


#endif //COMPLIER_ASTSTATEMENTNODE_H
