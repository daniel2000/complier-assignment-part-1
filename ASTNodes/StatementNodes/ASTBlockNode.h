//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTBLOCKNODE_H
#define COMPLIER_ASTBLOCKNODE_H


#include "ASTStatementNode.h"
#include <vector>

// Inherits ASTStatementNode
class ASTBlockNode : public ASTStatementNode{
public:
    // List of pointers to statement nodes
    vector<ASTStatementNode *> statementNodeList;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTBLOCKNODE_H
