//
// Created by attar on 07-May-20.
//

#ifndef COMPLIER_ASTWHILENODE_H
#define COMPLIER_ASTWHILENODE_H

#include "ASTStatementNode.h"
#include "..//ExpressionNodes/ASTExpressionNode.h"

// Inherits ASTStatementNode
class ASTWhileNode : public ASTStatementNode {
public:
    // Pointer to expression node
    ASTExpressionNode * expressionNode;
    // Pointer to block node
    ASTStatementNode * blockNode;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }

};


#endif //COMPLIER_ASTWHILENODE_H
