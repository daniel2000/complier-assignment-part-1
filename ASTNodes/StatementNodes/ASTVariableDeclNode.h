//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_ASTVARIABLEDECLNODE_H
#define COMPLIER_ASTVARIABLEDECLNODE_H

#include "ASTStatementNode.h"
#include "../ExpressionNodes/ASTExpressionNode.h"

// Inherits ASTStatementNode
class ASTVariableDeclNode : public ASTStatementNode{
public:
    // Pointer to identifier node
    ASTExpressionNode * identifierNode = nullptr;
    // Pointer to expression node
    ASTExpressionNode * expressionNode = nullptr;

    //Method for visitor design pattern
    virtual void accept(Visitor* v) override
    {
        v->visit(this);
    }
};


#endif //COMPLIER_ASTVARIABLEDECLNODE_H
