#include "SemanticAnalysisVisitor.h"

// Constructor
SemanticAnalysisVisitor::SemanticAnalysisVisitor() {
    // Initializing a new object of type SymbolTable
    auto symbolTblObject = new SymbolTable();
    this->symbolTable = symbolTblObject;
}

void SemanticAnalysisVisitor::visit(ASTBlockNode *blockNode) {
    // Going through the list of statement nodes in the block node
    for (auto it = blockNode->statementNodeList.begin(); it != blockNode->statementNodeList.end(); ++it) {
        (*it)->accept(this);
    }
}

void SemanticAnalysisVisitor::visit(ASTProgramNode * programNode) {
    // Adding a new scope
    symbolTable->push();

    // Going through the list of statement nodes in the program node
    for (auto it = programNode->statementNodeList.begin(); it != programNode->statementNodeList.end(); ++it) {
        (*it)->accept(this);
    }

    // Removing the scope
    symbolTable->pop();
}

void SemanticAnalysisVisitor::visit(ASTIdentifierNode * identifierNode) {
    id = true;

    tuple<string,string, int> idLookUpResult;

    // Looking up the function or the variable in the bindings of the current scope
    // A tuple is returned containing the type of the identifier and the scope number it was found in
    if (currentFunctionDeclarationOrCall) {
        idLookUpResult  = symbolTable->lookupFunction(identifierNode->id);
    } else {
        idLookUpResult  = symbolTable->lookupVariable(identifierNode->id);
    }


    currentId = identifierNode->id;

    if(currentVariableDeclaration || currentFunctionDeclarationOrCall) {
        // If the identifier was not found in the bindings
        if (get<0>(idLookUpResult).compare("notFound") == 0) {
            idFound = false;
            idFoundInParentScope = false;
            currentType = identifierNode->type;

        } else if (get<2>(idLookUpResult) != symbolTable->scopes.size()) {
            // The id was not found in the last scope
            // Than the same id was used but in a different scope but it can be redeclared in this scope
            idFound = false;
            idFoundInParentScope = true;
            currentType = identifierNode->type;

        } else {
            // Identifier already defined in this scope
            idFound = true;
            idFoundInParentScope = false;
            currentType = get<0>(idLookUpResult);
        }
    } else {
        // If not a variable declaration or a function declaration or call
        // If the identifier was not found
        if (get<0>(idLookUpResult).compare("notFound") == 0) {
            idFound = false;
            idFoundInParentScope = false;
            currentType = identifierNode->type;

        } else {
            // If the identifier was found
            idFound = true;
            idFoundInParentScope = false;
            currentType = get<0>(idLookUpResult);

        }
    }


    // Resetting for next call
    currentVariableDeclaration = false;
    currentFunctionDeclarationOrCall = false;

}

void SemanticAnalysisVisitor::visit(ASTVariableDeclNode * variableDeclNode){
    currentVariableDeclaration = true;

    // Visiting the identifier node
    variableDeclNode->identifierNode->accept(this);

    string variableType  = currentType;
    string variableId = currentId;

    // If an identifier was found but not found in the binding it is added to the binding
    // If it was found in the binding an error message is returned
    if(id && !idFound) {
        // Adding the variable to the map of the current scope
        symbolTable->insertVariable(currentId, currentType);
    } else {
        displayErrorMessage(currentId,true);
    }

    // Visiting the expression node
    variableDeclNode->expressionNode->accept(this);

    // Checking if an identifier was found and the binding was not found in any parent scope
    if(id && !idFound) {
        displayErrorMessage(currentId,false);
    }

    // If the variable type was auto, the variable type is now set to the type deduced from the expression, with the same value as before
    if (variableType.compare("auto") == 0) {
        symbolTable->scopes.top()->variableBindings[variableId] = make_pair(currentType,get<1>(symbolTable->scopes.top()->variableBindings[variableId]));
        variableType = currentType;
    }


    // Checking for no expression type mismatch
    if (currentType.compare("invalid") != 0) {
        // Checking for type mismatch between expression and identifier
        if(currentType.compare(variableType) != 0) {
            displayTypeMismatchErrorMessage(variableType,currentType);
        }
    }

}

void SemanticAnalysisVisitor::visit(ASTTermNode * termNode){

    set<string> types;

    // Adding a new set of strings to check for mismatch in the factors
    expressionTypesStack.push(types);

    // Going through the list of factor nodes in the term node
    for (auto it = termNode->factorNodesList.begin(); it != termNode->factorNodesList.end(); ++it) {
        (*it)->accept(this);

        // Checking if an identifier was found and the binding was not found in any parent scope
        if(id && !idFound) {
            displayErrorMessage(currentId,false);
        }

        // Inserting the type into the set
        expressionTypesStack.top().insert(currentType);
    }

    // Checking if there is an expression mismatch and storing the type for the identifier to match in currentType
    currentType = checkForExpressionTypeMismatch();


    // Here we can have an integer by integer division which results in a float
    for(auto it = termNode->multiplicativeOpList.begin(); it != termNode->multiplicativeOpList.end();++it) {
        if ((*it).compare("/") == 0 && currentType.compare("int") == 0){
            currentType = "float";
        }
    }

    expressionTypesStack.pop();

}

void SemanticAnalysisVisitor::visit(ASTSimpleExpressionNode * simpleExpressionNode){
    set<string> types;

    // Adding a new set of strings to check for mismatch in the different terms
    expressionTypesStack.push(types);

    // Going through the list of term nodes in the simple expression node
    for (auto it = simpleExpressionNode->termNodesList.begin(); it != simpleExpressionNode->termNodesList.end(); ++it) {
        (*it)->accept(this);

        // Checking if an identifier was found and the binding was not found in any parent scope
        if(id && !idFound) {
            displayErrorMessage(currentId,false);
        }

        // Inserting the type into the set
        expressionTypesStack.top().insert(currentType);
    }

    // Checking if there is an expression mismatch and storing the type for the identifier to match in currentType
    currentType = checkForExpressionTypeMismatch();

    expressionTypesStack.pop();
}

void SemanticAnalysisVisitor::visit(ASTExpressionNTNode * expressionNtNode){
    set<string> types;

    // Adding a new set of strings to check for mismatch in the different simple expression
    expressionTypesStack.push(types);

    // Going through the list of simple expression nodes in the expression nt node
    for (auto it = expressionNtNode->simpleExpressionNodesList.begin(); it != expressionNtNode->simpleExpressionNodesList.end(); ++it) {
        (*it)->accept(this);

        // Checking if an identifier was found and the binding was not found in any parent scope
        if(id && !idFound) {
            displayErrorMessage(currentId,false);
        }

        // Inserting the type into the set
        expressionTypesStack.top().insert(currentType);
    }

    // Checking if there is an expression mismatch and storing the type for the identifier to match in currentType
    currentType = checkForExpressionTypeMismatch();

    expressionTypesStack.pop();

    // If the expressions are of the same type and a relational operator exists between them, their result is a boolean
    if (currentType.compare("invalid") != 0) {
        currentType = "bool";
    }
}

void SemanticAnalysisVisitor::visit(ASTLiteralNode * literalNode){
    // Storing the type of the literal for type mismatch checking
    currentType = literalNode->type;
    // An identifier hasn't been found hence id = false
    id = false;
}

void SemanticAnalysisVisitor::visit(ASTFunctionCallNode * functionCallNode){
    currentFunctionDeclarationOrCall = true;
    // Visiting the identifier node
    functionCallNode->identifierNode->accept(this);
    string functionName = currentId;

    // If an identifier was found but not found in the binding an error message is returned
    if(id && (!idFound && !idFoundInParentScope)) {
        displayErrorMessage(currentId,false);
    }

    // Adding the current id to a stack of current function ids
    currentFunctionId.push(currentId);

    // Visiting the actual Params node
    functionCallNode->actualParamsNode->accept(this);

    // Removing the last function id from the stack
    currentFunctionId.pop();

    // Looking up the function and storing its return type in current type
    tuple<string, string, int> lookUpReturn = symbolTable->lookupFunction(functionName);
    currentType = get<0>(lookUpReturn);

}

void SemanticAnalysisVisitor::visit(ASTActualParamsNode * actualParamsNode){
    vector<string> actualParamsList;

    // Searching for the function in the bindings
    tuple<string, string, int> lookUpReturn = symbolTable->lookupFunction(currentFunctionId.top());

    // Declaring a copy of the stack of scopes
    stack<Scope *> tempScopesStack = symbolTable->scopes;

    // Obtaining the scope which the binding was found in
    while(tempScopesStack.size() != get<2>(lookUpReturn)) {
        tempScopesStack.pop();
    }

    // Obtaining the value of the binding with the functionId as key
    // The value is a tuple of a string and list of string, representing the return type and formal parameters respectively
    auto tupleReturnTypeParameterTypes = tempScopesStack.top()->functionBindings.at(currentFunctionId.top());

    int matchedParameters = 0;

    // Going through the list of expression nodes in the expression actual params node
    for (auto it = actualParamsNode->expressionNodeList.begin(); it != actualParamsNode->expressionNodeList.end(); ++it) {
        (*it)->accept(this);

        // Checking if an identifier was found and the binding was not found in any parent scope
        if(id && !idFound) {
            displayErrorMessage(currentId,false);
        }

        // Checking for no expression type mismatch
        if (currentType.compare("invalid") != 0) {

            // Adding the found parameter to a list
            actualParamsList.push_back(currentType);

            // Checking if the number of found parameters is equal to the number of formal parameters (required by the function)
            if (actualParamsList.size() == get<1>(tupleReturnTypeParameterTypes).size()) {
                // To check if they are the same type and in the same order
                int counter = 0;
                for (auto it = actualParamsList.begin(); it != actualParamsList.end(); ++it) {
                    // Checking that the actual params and formal params match
                    if((*it).compare(get<1>(tupleReturnTypeParameterTypes)[counter]) == 0) {
                        matchedParameters++;
                    }
                    counter++;
                }

            }
        }


    }

    // Checking that the number of parameters matched is equal to the number of formal parameters
    // Also checking that all the actual parameters were matched
    if (matchedParameters != get<1>(tupleReturnTypeParameterTypes).size() || actualParamsList.size() != matchedParameters) {
        displayActualParamsError(currentFunctionId.top(),get<1>(tupleReturnTypeParameterTypes),actualParamsList);
    }


}

void SemanticAnalysisVisitor::visit(ASTAssignmentNode * assignmentNode){

    // Visiting the identifier node
    assignmentNode->identifierNode->accept(this);
    string identifierType = currentType;

    if(id && !idFound) {
        // Since the variable is not found, an error is displayed
        displayErrorMessage(currentId,false);
    }

    // Visiting the expression node
    assignmentNode->expressionNode->accept(this);

    // Checking if an identifier was found and the binding was not found in any parent scope
    if(id && !idFound) {
        displayErrorMessage(currentId,false);
    }

    // Checking for no expression type mismatch
    if (currentType.compare("invalid") != 0) {
        // Checking for type mismatch between expression and identifier
        if(currentType.compare(identifierType) != 0) {
            displayTypeMismatchErrorMessage(currentType,identifierType);
        }
    }
}

void SemanticAnalysisVisitor::visit(ASTPrintNode * printNode){

    // Visiting the expression node
    printNode->expressionNode->accept(this);

    // Checking if an identifier was found and the binding was not found in any parent scope
    if(id && !idFound) {
        // Since the variable is not found, an error is displayed
        displayErrorMessage(currentId,false);
    }

}

void SemanticAnalysisVisitor::visit(ASTIfNode * ifNode){
    // Visiting the expression node
    ifNode->expressionNode->accept(this);

    // Checking if an identifier was found and the binding was not found in any parent scope
    if(id && !idFound) {
        // Since the variable is not found, an error is displayed
        displayErrorMessage(currentId,false);
    }

    // Checking that the expression is of type boolean
    if (currentType.compare("bool") != 0) {
        displayNotBooleanError("if");
    }

    // Adding a new scope
    symbolTable->push();

    // Visiting the true block node
    ifNode->trueBlockNode->accept(this);

    // Removing the scope
    symbolTable->pop();

    // Adding a new scope
    symbolTable->push();

    // Since the else part is optional
    if(ifNode->falseBlockNode != nullptr) {
        // Visiting the true block node
        ifNode->falseBlockNode->accept(this);
    }


    // Removing the scope
    symbolTable->pop();


}

void SemanticAnalysisVisitor::visit(ASTForNode * forNode){
    // This variable declaration can be done between the for loop and hence a new scope is not pushed until the block node
    if ( forNode->variableDeclNode != nullptr) {
        // Visiting the variable declaration node
        forNode->variableDeclNode->accept(this);
    }


    // Visiting the expresion node
    forNode->expressionNode->accept(this);

    // Checking if an identifier was found and the binding was not found in any parent scope
    if(id && !idFound) {
        // Since the variable is not found, an error is displayed
        displayErrorMessage(currentId,false);
    }

    // Making sure that the expression results in a boolean
    if (currentType.compare("bool") != 0) {
        displayNotBooleanError("for");
    }


    // Since assignment is optional it is checked not to be null
    if (forNode->assignmentNode != nullptr){
        // Visiting the assignment node
        forNode->assignmentNode->accept(this);
    }

    // Adding a new scope
    symbolTable->push();

    // Visiting the block node
    forNode->blockNode->accept(this);

    // Removing the scope
    symbolTable->pop();

}

void SemanticAnalysisVisitor::visit(ASTFormalParamNode * formalParamNode){
    // This is so if the same parameter identifier is used is the previous scope, it can still be used here
    currentVariableDeclaration = true;
    formalParamNode->identifierNode->accept(this);

    // Adding an identifier binding to the current scope so this variable can be used by the rest of the function
    if(id && !idFound) {
        // Adding the variable to the map of the current scope
        symbolTable->insertVariable(currentId, currentType);
    } else {
        displayErrorMessage(currentId,true);
    }

    // Adding the parameter type to the binding of the function
    symbolTable->addParameterToFunction(currentFunctionId.top(),currentType);
}

void SemanticAnalysisVisitor::visit(ASTFormalParamsNode * formalParamsNode){
    // Going through the formal param nodes in the expression formal params node
    for (auto it = formalParamsNode->formalParamNodesList.begin(); it != formalParamsNode->formalParamNodesList.end(); ++it) {
        (*it)->accept(this);
    }
}

void SemanticAnalysisVisitor::visit(ASTFunctionDeclNode * functionDeclNode){
    currentFunctionDeclarationOrCall = true;

    // Visiting the identifier Node
    functionDeclNode->identifierNode->accept(this);
    // In this case this is the return type of the function
    //string identifierType = currentType;

    // If the variable identifier wasn't found, a new binding is added with the identifier and the function return type
    if(id && !idFound) {
        // Adding the variable to the map of the current scope
        symbolTable->insertFunction(currentId, functionDeclNode->type);
    } else {
        displayErrorMessage(currentId,true);
    }

    currentFunctionId.push(currentId);

    // Adding a new scope
    symbolTable->push();

    // If the function has formal parameters these are visited
    if (functionDeclNode->formalParamsNode != nullptr) {
        // Visiting the formal params node
        functionDeclNode->formalParamsNode->accept(this);
    }


    // Visiting the block node
    functionDeclNode->blockNode->accept(this);

    // Removing the scope
    symbolTable->pop();



}

void SemanticAnalysisVisitor::visit(ASTReturnNode * returnNode){
    // Visiting the expression node
    returnNode->expressionNode->accept(this);

    // Checking if an identifier was found and the binding was not found in any parent scope
    if(id && !idFound) {
        displayErrorMessage(currentId,false);
    }


    // Checking the return type of the function we are returning from
    auto lookUpReturn = symbolTable->lookupFunction(currentFunctionId.top());


    // Checking for no expression type mismatch
    if (currentType.compare("invalid") != 0) {
        // If the function has auto return type
        if (get<0>(lookUpReturn).compare("auto") == 0){
            // Changing the type of the function to the one derived from the above expression
            symbolTable->changeFunctionType(currentFunctionId.top(),currentType);

        }
        else if(currentType.compare(get<0>(lookUpReturn)) != 0) {
            // Checking for type mismatch between expression and return type
            displayTypeMismatchErrorMessage(get<0>(lookUpReturn),currentType);
        }
    }

    currentFunctionId.pop();
}

void SemanticAnalysisVisitor::visit(ASTUnaryNode * unaryNode){
    //Visiting expression node
    unaryNode->expressionNode->accept(this);

    // Checking if an identifier was found and the binding was not found in any parent scope
    if(id && !idFound) {
        displayErrorMessage(currentId,false);
    }
}

void SemanticAnalysisVisitor::visit(ASTWhileNode * whileNode) {
    // Visiting the expression node
    whileNode->expressionNode->accept(this);

    // Checking if an identifier was found and the binding was not found in any parent scope
    if(id && !idFound) {
        displayErrorMessage(currentId,false);
    }

    // Checking that the expression node is of type boolean
    if (currentType.compare("bool") != 0) {
        displayNotBooleanError("while");
    }

    // Adding a new scope
    symbolTable->push();

    // Visiting the block node
    whileNode->blockNode->accept(this);

    // Removing a scope
    symbolTable->pop();
}

void SemanticAnalysisVisitor::displayErrorMessage(string id, bool duplicate) {
    // The error message if a variable was not declared or declared twice
    if(!semanticError) {
        semanticError = true;
        if (duplicate){
            cout << "Semantic Error: " << id << " has already been declared!"<< endl;
        } else {
            cout << "Semantic Error: " << id << " is not declared!"<< endl;
        }
    }

}

void SemanticAnalysisVisitor::displayTypeMismatchErrorMessage(string type1, string type2) {
    // Error for type mismatch between identifier and expression
    if(!semanticError){
        semanticError = true;
        cout << "Semantic Error | Type Mismatch: " << type1 << " != " << type2 << endl;
    }

}

void SemanticAnalysisVisitor::displayTypeMismatchInExpression(){
    // Error for type mismatch between factors of the expression itself
    if (!semanticError) {
        semanticError = true;
        string types;
        int typeCounter = 0;

        // Adding all the types found in the expression to a list
        for (auto it=expressionTypesStack.top().begin(); it != expressionTypesStack.top().end(); ++it) {
            // Adding all the types found in the expression to a list
            types += *it;

            if (typeCounter != expressionTypesStack.top().size() - 1 ) {
                types += " and ";
            }

            typeCounter++;
        }

        cout << "Semantic Error | Type Mismatch in expression having types: "<< types << endl;
    }


}

string SemanticAnalysisVisitor::checkForExpressionTypeMismatch() {
    // If only 1 type has been added to the set, the expression has no type mismatch
    if(expressionTypesStack.top().size() == 1) {
        // No mismatch in expression
        auto firstItem = expressionTypesStack.top().begin();

        // Returning the type the identifier should match to
        return *firstItem;
    } else if (expressionTypesStack.top().size() == 2 && expressionTypesStack.top().find("int") != expressionTypesStack.top().end() && expressionTypesStack.top().find("float") != expressionTypesStack.top().end()) {
        // If two types of identifiers have been found (int and float) and the declared identifier is a float
        // There is no type mismatch in the expression

        return "float";
    } else {
        // Expression mismatch
        displayTypeMismatchInExpression();

        return "invalid";
    }
}

void SemanticAnalysisVisitor::displayNotBooleanError(string statement){
    // Error when the expression is not a boolean where required
    if (!semanticError) {
        semanticError = true;
        cout << "Expected Boolean expression in "<< statement <<" loop" << endl;
    }
}

void SemanticAnalysisVisitor::displayActualParamsError(string functionId, vector<string> formalParams, vector<string> actualParams){
    // Error when the actual params and formal params dont match
    if(!semanticError){
        semanticError = true;

        cout<< "Parameter mismatch when calling function " << functionId <<endl;
        cout<< "Expected Parameters: ";

        int counter = 0;
        // Displaying the expected parameters
        for (auto it = formalParams.begin(); it != formalParams.end(); ++it) {
            cout << (*it);

            if(counter != formalParams.size() - 1) {
                cout << " and ";
            }
            counter++;
        }


        if (counter == 0) {
            cout << "NONE" <<endl;
        } else  {
            cout<<""<<endl;
        }
        counter = 0;

        cout<< "Passed Parameters: ";

        // Displaying the actual parameters
        for (auto it = actualParams.begin(); it != actualParams.end(); ++it) {
            cout << (*it);

            if(counter != actualParams.size() - 1) {
                cout << " and ";
            }
            counter++;
        }

        if (counter == 0) {
            cout << "NONE" <<endl;
        } else  {
            cout<<""<<endl;
        }
    }
}
