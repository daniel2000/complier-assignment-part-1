//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_VISITOR_H
#define COMPLIER_VISITOR_H

// Using forward declaration to get the various classes

class ASTExpressionNode;
class ASTStatementNode;
class ASTIdentifierNode;
class ASTVariableDeclNode;
class ASTLiteralNode;
class ASTFunctionCallNode;
class ASTSimpleExpressionNode;
class ASTTermNode;
class ASTExpressionNTNode;
class ASTBlockNode;
class ASTProgramNode;
class ASTActualParamsNode;
class ASTAssignmentNode;
class ASTPrintNode;
class ASTReturnNode;
class ASTIfNode;
class ASTForNode;
class ASTWhileNode;
class ASTFormalParamNode;
class ASTFormalParamsNode;
class ASTFunctionDeclNode;
class ASTUnaryNode;



class Visitor {
public:

    // A visit function for each different AST node
    // These are virtual function since these are never used but overridden by other function in the different classes
    virtual void visit(ASTIdentifierNode * identifierNode) = 0;
    virtual void visit(ASTVariableDeclNode * variableDeclNode) = 0;
    virtual void visit(ASTLiteralNode * literalNode) = 0;
    virtual void visit(ASTFunctionCallNode * functionCallNode) = 0;
    virtual void visit(ASTSimpleExpressionNode * simpleExpressionNode) = 0;
    virtual void visit(ASTTermNode * termNode) = 0;
    virtual void visit(ASTExpressionNTNode * expressionNtNode) = 0;
    virtual void visit(ASTBlockNode * blockNode) = 0;
    virtual void visit(ASTProgramNode * programNode) = 0;
    virtual void visit(ASTActualParamsNode * actualParamsNode) = 0;
    virtual void visit(ASTAssignmentNode * assignmentNode) = 0;
    virtual void visit(ASTPrintNode * printNode) = 0;
    virtual void visit(ASTReturnNode * returnNode) = 0;
    virtual void visit(ASTIfNode * ifNode) = 0;
    virtual void visit(ASTForNode * forNode) = 0;
    virtual void visit(ASTWhileNode * whileNode) = 0;
    virtual void visit(ASTFormalParamNode * formalParamNode) = 0;
    virtual void visit(ASTFormalParamsNode * formalParamsNode) = 0;
    virtual void visit(ASTFunctionDeclNode * functionDeclNode) = 0;
    virtual void visit(ASTUnaryNode * unaryNode) = 0;

};


#endif //COMPLIER_VISITOR_H
