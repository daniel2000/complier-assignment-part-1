//
// Created by attar on 10-May-20.
//

#ifndef COMPLIER_SEMANTICANALYSISVISITOR_H
#define COMPLIER_SEMANTICANALYSISVISITOR_H

#include "Visitor.h"
#include "..//SymbolTable.h"
#include <set>


#include "../ASTNodes/ExpressionNodes/ASTIdentifierNode.h"
#include "../ASTNodes/StatementNodes/ASTProgramNode.h"
#include "../ASTNodes/StatementNodes/ASTWhileNode.h"
#include "../ASTNodes/StatementNodes/ASTBlockNode.h"
#include "../ASTNodes/StatementNodes/ASTPrintNode.h"
#include "../ASTNodes/StatementNodes/ASTVariableDeclNode.h"
#include "../ASTNodes/ExpressionNodes/ASTTermNode.h"
#include "../ASTNodes/ExpressionNodes/ASTSimpleExpressionNode.h"
#include "../ASTNodes/ExpressionNodes/ASTExpressionNTNode.h"
#include "../ASTNodes/ExpressionNodes/ASTLiteralNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFunctionCallNode.h"
#include "../ASTNodes/ExpressionNodes/ASTActualParamsNode.h"
#include "../ASTNodes/StatementNodes/ASTAssignmentNode.h"
#include "../ASTNodes/StatementNodes/ASTReturnNode.h"
#include "../ASTNodes/StatementNodes/ASTIfNode.h"
#include "../ASTNodes/StatementNodes/ASTForNode.h"
#include "../ASTNodes/ExpressionNodes/ASTUnaryNode.h"
#include "../ASTNodes/StatementNodes/ASTFunctionDeclNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFormalParamsNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFormalParamNode.h"

class SemanticAnalysisVisitor :  public Visitor{

public:
    //Constructor
    SemanticAnalysisVisitor();

    // Pointer to object of type SymbolTable
    SymbolTable * symbolTable;

    // Verify that the identifier binding was found
    bool idFound = false;

    // Verify that an identifier was found in a parent scope
    bool idFoundInParentScope = false;

    // Verify that a semantic error has been found
    bool semanticError = false;

    // Verify that an identifier has been found
    bool id = false;

    // Verify that a variable declaration is being held
    bool currentVariableDeclaration = false;

    // Verify that a function declaration or call is being held
    bool currentFunctionDeclarationOrCall = false;

    // Storing various parameters to check that types and identifiers match
    string currentId = "";
    string currentType = "";

    // Stack holding the current function id
    stack<string> currentFunctionId;

    // Stack of set of string, storing expression type
    stack<set<string>> expressionTypesStack;

    // Checking for type mismatch within an expression
    string checkForExpressionTypeMismatch();

    // The error message if a variable was not declared or declared twice
    void displayErrorMessage(string id, bool duplicate);

    // Error for type mismatch between identifier and expression
    void displayTypeMismatchErrorMessage(string type1, string type2);

    // Error for type mismatch between factors of the expression itself
    void displayTypeMismatchInExpression();

    // Error when the expression is not a boolean where required
    void displayNotBooleanError(string statement);

    // Error when the actual params and formal params dont match
    void displayActualParamsError(string functionId, vector<string> formalParams, vector<string> actualParams);


    // Visit method for each different AST node
    virtual void visit(ASTBlockNode * blockNode) override;

    virtual void visit(ASTProgramNode * programNode) override;

    virtual void visit(ASTIdentifierNode * identifierNode) override ;

    virtual void visit(ASTWhileNode * whileNode) override;

    virtual void visit(ASTVariableDeclNode * variableDeclNode) override ;

    virtual void visit(ASTTermNode * termNode) override;

    virtual void visit(ASTSimpleExpressionNode * simpleExpressionNode) override ;

    virtual void visit(ASTExpressionNTNode * expressionNtNode) override ;

    virtual void visit(ASTLiteralNode * literalNode) override ;

    virtual void visit(ASTFunctionCallNode * functionCallNode) override ;

    virtual void visit(ASTActualParamsNode * actualParamsNode) override;

    virtual void visit(ASTAssignmentNode * assignmentNode) override ;

    virtual void visit(ASTPrintNode * printNode) override;

    virtual void visit(ASTReturnNode * returnNode) override;

    virtual void visit(ASTIfNode * ifNode) override;

    virtual void visit(ASTForNode * forNode) override;

    virtual void visit(ASTFormalParamNode * formalParamNode) override;

    virtual void visit(ASTFormalParamsNode * formalParamsNode) override;

    virtual void visit(ASTFunctionDeclNode * functionDeclNode) override;

    virtual void visit(ASTUnaryNode * unaryNode) override ;

};


#endif //COMPLIER_SEMANTICANALYSISVISITOR_H
