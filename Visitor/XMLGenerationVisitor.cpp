//
// Created by attar on 06-May-20.
//

#include "XMLGenerationVisitor.h"

// Constructor for this class which accepts an ofstream pointer, for storing the generated XML in a file
XMLGenerationVisitor::XMLGenerationVisitor(ofstream * outfile){
    // The pointer to an object of type ofstream is stored in the class variable of the same type
    this->outfile = outfile;
}

// Increasing the indentation amount
void XMLGenerationVisitor::IncreaseIndent(){
    // Adding another indent (tab)
    indent.append("\t");
}

// Decreasing the indentation amount
void XMLGenerationVisitor::DecreaseIndent(){
    // Removing the last character from the string
    indent.pop_back();
}

void XMLGenerationVisitor::visit(ASTBlockNode * blockNode) {
    // Start Block Node Tag
    *outfile << indent <<"<"<< blockNode->name <<">" << endl;
    IncreaseIndent();

    // Going through the list of statementNodes in the list of statementNodes in the BlockNode
    for (auto it = blockNode->statementNodeList.begin(); it != blockNode->statementNodeList.end(); ++it) {
        (*it)->accept(this);
    }


    DecreaseIndent();
    // End Block Node Tag
    *outfile << indent <<"</"<< blockNode->name <<">" << endl;

}

void XMLGenerationVisitor::visit(ASTProgramNode * programNode) {
    //Program Node

    // Start Program Node Tag
    *outfile <<indent << "<"<< programNode->name <<">" << endl;
    IncreaseIndent();

    // Going through the list of statementNodes in the list of statementNodes in the ProgramNode
    for (auto it = programNode->statementNodeList.begin(); it != programNode->statementNodeList.end(); ++it) {
        (*it)->accept(this);
    }

    DecreaseIndent();
    // End Program Node Tag
    *outfile << indent << "</"<< programNode->name <<">" << endl;

}

void XMLGenerationVisitor::visit(ASTVariableDeclNode * variableDeclNode)  {
    // Start Variable Declaration Node Tag
    *outfile << indent <<"<"<< variableDeclNode->name <<">" << endl;
    IncreaseIndent();

    // Visiting thye identifier and expression node
    variableDeclNode->identifierNode->accept(this);
    variableDeclNode->expressionNode->accept(this);

    DecreaseIndent();
    // End Variable Declaration Node Tag
    *outfile << indent <<"</"<< variableDeclNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTTermNode * termNode) {
    string multiplicativeOps = "";
    int tempMultipliveOpCounter = 0;

    // Going through all the multiplicative op
    for (auto it = termNode->multiplicativeOpList.begin(); it != termNode->multiplicativeOpList.end(); ++it){

        tempMultipliveOpCounter++;

        // Adding each of the multiplicativeOps to a string
        multiplicativeOps += (*it);

        // Adding a common if not the last multiplicativeOp
        if (tempMultipliveOpCounter != termNode->multiplicativeOpList.size())
            multiplicativeOps += ",";
    }

    // Term Node Tag
    *outfile << indent <<"<"<< termNode->name <<" multiplicativeOps=\""<< multiplicativeOps <<"\">"<< endl;
    IncreaseIndent();

    //Going thorough all the factor nodes in the list of factorNode in the termNode
    for (int i = 0; i<termNode->factorNodesList.size(); i++) {
    termNode->factorNodesList.at(i)->accept(this);
    }

    DecreaseIndent();
    // Closing Term Node Tag
    *outfile << indent <<"</"<< termNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTSimpleExpressionNode * simpleExpressionNode) {
    int tempAdditiveCounter =0;
    string additiveOps = "";

    // Going through each additive operator in the lists of additive operators
    for (auto it = simpleExpressionNode->additiveOpList.begin(); it != simpleExpressionNode->additiveOpList.end(); ++it){
        tempAdditiveCounter++;

        // Adding each additive operator to a string
        additiveOps += (*it);

        // Adding a common if not the last additiveOP
        if (tempAdditiveCounter != simpleExpressionNode->additiveOpList.size()) {
            additiveOps += ",";
        }

    }

    // Simple Expression Node Tag
    *outfile << indent <<"<"<< simpleExpressionNode->name <<" additiveOps=\""<< additiveOps <<"\">"<< endl;
    IncreaseIndent();

    //Standard loop
    for (int i = 0; i<simpleExpressionNode->termNodesList.size(); i++) {

        simpleExpressionNode->termNodesList.at(i)->accept(this);
    }

    DecreaseIndent();
    // Closing Simple Expression Node Tag
    *outfile << indent <<"</"<< simpleExpressionNode->name <<">" << endl;

}

void XMLGenerationVisitor::visit(ASTExpressionNTNode * expressionNtNode) {
    int tempRelationalOps = 0;
    string relationalOps = "";

    // Adding all the additive operators to a string
    for (auto it = expressionNtNode->relationalOpList.begin(); it != expressionNtNode->relationalOpList.end(); ++it){
        //Incrementing the counter of the number of relationalOps
        tempRelationalOps++;

        // Adding the relationalOps to the string
        relationalOps += (*it);

        // Adding a common if not the last relationalOp
        if(tempRelationalOps != expressionNtNode->relationalOpList.size()){
            relationalOps += ",";
        }

    }

    // Simple Expression Node Tag
    *outfile << indent <<"<"<< expressionNtNode->name <<" relationalOps=\""<< relationalOps <<"\">"<< endl;
    IncreaseIndent();

    //Visiting each simple expression node in the list of simple expression nodes in the expressionNTnode
    for (int i = 0; i<expressionNtNode->simpleExpressionNodesList.size(); i++) {
        expressionNtNode->simpleExpressionNodesList.at(i)->accept(this);
    }

    DecreaseIndent();
    // Closing Expression Node Tag
    *outfile << indent <<"</"<< expressionNtNode->name <<">" << endl;

}

void XMLGenerationVisitor::visit(ASTLiteralNode * literalNode) {
    // Checking the type of literal found and adding the appropriate XML
    if (literalNode->type.compare("bool") == 0) {
        // Literal Node Tag
        *outfile << indent <<"<Boolean>" << literalNode->value<<"</Boolean>" << endl;
    } else if (literalNode->type.compare("int") == 0) {
        // Literal Node Tag
        *outfile << indent <<"<IntConst>" << literalNode->value<<"</IntConst>" << endl;
    } else if (literalNode->type.compare("float") == 0) {
        // Literal Node Tag
        *outfile << indent <<"<FloatConst>" << literalNode->value<<"</FloatConst>" << endl;
    }
}

void XMLGenerationVisitor::visit(ASTIdentifierNode * identifierNode) {
    // Checking if the type variable in the identifier node is empty and different XML versions are outputted based off this
    if (identifierNode->type.empty()) {
        // Identifier Node Tag without type
        *outfile << indent <<"<"<< identifierNode->name <<">" << identifierNode->id<<"</Identifier>" << endl;
    } else {
        // Identifier Node Tag with type
        *outfile << indent <<"<"<< identifierNode->name <<" Type=\"" << identifierNode->type << "\">" << identifierNode->id<<"</Identifier>" << endl;
    }
}

void XMLGenerationVisitor::visit(ASTFunctionCallNode * functionCallNode){
    // Opening Function Call Node Tag
    *outfile << indent <<"<"<< functionCallNode->name <<">" << endl;
    IncreaseIndent();

    // Visiting the identifier node
    functionCallNode->identifierNode->accept(this);

    // Checking if the actualParamsNode is initialized (since it is optional_ and visiting it
    if(functionCallNode->actualParamsNode != nullptr) {
        functionCallNode->actualParamsNode->accept(this);
    }

    DecreaseIndent();
    // Closing Function Call Node Tag
    *outfile << indent <<"</"<< functionCallNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTActualParamsNode * actualParamsNode) {
    // Opening ActualParams Node Tag
    *outfile << indent <<"<"<< actualParamsNode->name <<">" << endl;
    IncreaseIndent();

    //Visiting each expression node in the list of expression node in the actual params node
    for (int i = 0; i<actualParamsNode->expressionNodeList.size(); i++) {
        actualParamsNode->expressionNodeList.at(i)->accept(this);
    }

    DecreaseIndent();
    // Closing ActualParams Node Tag
    *outfile << indent <<"</"<< actualParamsNode->name <<">" << endl;

}

void XMLGenerationVisitor::visit(ASTAssignmentNode * assignmentNode){
    // Opening Assignment Node Tag
    *outfile << indent <<"<"<< assignmentNode->name <<">" << endl;
    IncreaseIndent();

    // Visiting the identifier and expression node
    assignmentNode->identifierNode->accept(this);
    assignmentNode->expressionNode->accept(this);

    DecreaseIndent();
    // Closing Assignment Node Tag
    *outfile << indent <<"</"<< assignmentNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTPrintNode * printNode) {
    // Opening Print Node Tag
    *outfile << indent <<"<"<< printNode->name <<">" << endl;
    IncreaseIndent();

    // Visiting the expression node
    printNode->expressionNode->accept(this);

    DecreaseIndent();
    // Closing Print Node Tag
    *outfile << indent <<"</"<< printNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTReturnNode * returnNode) {
    // Opening Return Node Tag
    *outfile << indent <<"<"<< returnNode->name <<">" << endl;
    IncreaseIndent();

    //Visiting the expression node
    returnNode->expressionNode->accept(this);

    DecreaseIndent();
    // Closing Return Node Tag
    *outfile << indent <<"</"<< returnNode->name <<">" << endl;
}


void XMLGenerationVisitor::visit(ASTIfNode * ifNode) {
    // Opening If Node Tag
    *outfile << indent <<"<"<< ifNode->name <<">" << endl;
    IncreaseIndent();

    //Visiting the expresion node
    ifNode->expressionNode->accept(this);

    //Visiting the true block node
    ifNode->trueBlockNode->accept(this);

    if(ifNode->falseBlockNode != nullptr) {
        //Visiting the false block node
        ifNode->falseBlockNode->accept(this);
    }

    DecreaseIndent();
    // Closing If Node Tag
    *outfile << indent <<"</"<< ifNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTForNode * forNode) {
    // Opening For Node Tag
    *outfile << indent <<"<"<< forNode->name <<">" << endl;
    IncreaseIndent();

    // Checking if the variable declaration node is initialized and visiting it
    if (forNode->variableDeclNode != nullptr){
        forNode->variableDeclNode->accept(this);
    }

    // Visiting the expression node
    forNode->expressionNode->accept(this);

    // Checking if the assignment node is initialized and visiting it
    if ( forNode->assignmentNode != nullptr){
        forNode->assignmentNode->accept(this);
    }

    // Visiting the block node
    forNode->blockNode->accept(this);

    DecreaseIndent();
    // Closing For Node Tag
    *outfile << indent <<"</"<< forNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTWhileNode * whileNode) {
    // Opening While Node Tag
    *outfile << indent <<"<"<< whileNode->name <<">" << endl;
    IncreaseIndent();

    // Visiting the expression node
    whileNode->expressionNode->accept(this);
    // Visiting the block node
    whileNode->blockNode->accept(this);

    DecreaseIndent();
    // Closing While Node Tag
    *outfile << indent <<"</"<< whileNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTFormalParamNode * formalParamNode) {
    // Opening FormalParam Node Tag
    *outfile << indent <<"<"<< formalParamNode->name <<">" << endl;
    IncreaseIndent();

    // Visiting the identifier node
    formalParamNode->identifierNode->accept(this);

    DecreaseIndent();
    // Closing FormalParam Tag
    *outfile << indent <<"</"<< formalParamNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTFormalParamsNode * formalParamsNode) {
    // Opening FormalParams Node Tag
    *outfile << indent <<"<"<< formalParamsNode->name <<">" << endl;
    IncreaseIndent();

    // Going through the list of formal param in the formalParamsNode
    for (auto it = formalParamsNode->formalParamNodesList.begin(); it != formalParamsNode->formalParamNodesList.end(); ++it) {
        (*it)->accept(this);
    }


    DecreaseIndent();
    // Closing FormalParams Tag
    *outfile << indent <<"</"<< formalParamsNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTFunctionDeclNode * functionDeclNode) {
    // Opening FunctionDecl Node Tag
    *outfile << indent <<"<"<< functionDeclNode->name <<" ReturnType=\"" << functionDeclNode->type <<"\">" << endl;
    IncreaseIndent();

    //Visiting the identifier Node
    functionDeclNode->identifierNode->accept(this);

    // Checking if the formalParamsNode has a null pointer since this is optional in the code
    if ( functionDeclNode->formalParamsNode != nullptr){
        // Visiting the formal Params Node
        functionDeclNode->formalParamsNode->accept(this);
    }

    // Visiting the block node
    functionDeclNode->blockNode->accept(this);

    DecreaseIndent();
    // Closing FunctionDecl Tag
    *outfile << indent <<"</"<< functionDeclNode->name <<">" << endl;
}

void XMLGenerationVisitor::visit(ASTUnaryNode * unaryNode) {
    // Opening Unary Node Tag
    *outfile << indent <<"<"<< unaryNode->name <<" Type=\"" << unaryNode->oppositeOperator <<"\">" << endl;
    IncreaseIndent();

    // Visiting the expression node
    unaryNode->expressionNode->accept(this);

    DecreaseIndent();
    // Closing Unary Tag
    *outfile << indent <<"</"<< unaryNode->name <<">" << endl;
}