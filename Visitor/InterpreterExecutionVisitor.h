//
// Created by attar on 12-May-20.
//

#ifndef COMPLIER_INTERPRETEREXECUTIONVISITOR_H
#define COMPLIER_INTERPRETEREXECUTIONVISITOR_H


#include "Visitor.h"
#include "..//SymbolTable.h"
#include <set>
#include <iostream>
#include <algorithm>
#include <list>

#include "../ASTNodes/ExpressionNodes/ASTIdentifierNode.h"
#include "../ASTNodes/StatementNodes/ASTProgramNode.h"
#include "../ASTNodes/StatementNodes/ASTWhileNode.h"
#include "../ASTNodes/StatementNodes/ASTBlockNode.h"
#include "../ASTNodes/StatementNodes/ASTPrintNode.h"
#include "../ASTNodes/StatementNodes/ASTVariableDeclNode.h"
#include "../ASTNodes/ExpressionNodes/ASTTermNode.h"
#include "../ASTNodes/ExpressionNodes/ASTSimpleExpressionNode.h"
#include "../ASTNodes/ExpressionNodes/ASTExpressionNTNode.h"
#include "../ASTNodes/ExpressionNodes/ASTLiteralNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFunctionCallNode.h"
#include "../ASTNodes/ExpressionNodes/ASTActualParamsNode.h"
#include "../ASTNodes/StatementNodes/ASTAssignmentNode.h"
#include "../ASTNodes/StatementNodes/ASTReturnNode.h"
#include "../ASTNodes/StatementNodes/ASTIfNode.h"
#include "../ASTNodes/StatementNodes/ASTForNode.h"
#include "../ASTNodes/ExpressionNodes/ASTUnaryNode.h"
#include "../ASTNodes/StatementNodes/ASTFunctionDeclNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFormalParamsNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFormalParamNode.h"

class InterpreterExecutionVisitor : public Visitor {
public:
    //Constructor
    InterpreterExecutionVisitor();

    // Pointer to object of type SymbolTable
    SymbolTable * symbolTable;

    // Verify that the identifier binding was found
    bool idFound = false;

    // Verify that the id was found in a parent scope
    bool idFoundInParentScope = false;

    // Verify that a semantic error has been found
    bool semanticError = false;

    // Verify that an identifier has been found
    bool id = false;

    // Verify that a variable declaration is being held
    bool currentVariableDeclaration = false;

    // Verify that a function declaration or call is being held
    bool currentFunctionDeclarationOrCall = false;

    //Stack of boolean whether the current operation is a function call
    stack<bool> currentFunctionCall;

    //Stack of boolean whether the current operation is a function declaration
    stack<bool> currentFunctionDeclaration;

    //Stack storing the current function id
    stack<string> currentFunctionId;

    // Check whether a function call occured
    bool calledFromFunctionCall = false;


    // A map which stores the function id as the key and the pointer to the function declaration node as the value
    unordered_map<string, ASTFunctionDeclNode*> functionIdPointermap;

    // Storing various parameters to check that types and identifiers match
    string currentId = "";
    string currentType = "";
    string currentValue = "";

    // Checking if a calcualtion error has occured
    bool calculationError = false;

    // Stack of set of string, storing expression type and value
    stack<vector<pair<string, string>>> expressionTypesValuesStack;

    // Post fix calculator to carry out the operation
    pair<string, string> postFixCalculator();

    // Stack of List to store the actual params of the current function call
    stack<vector<string>> currentFunctionActualParamsValues;

    // The error message if a variable was not declared or declared twice
    void displayErrorMessage(string id, bool duplicate);

    // Error for type mismatch between identifier and expression
    void displayTypeMismatchErrorMessage(string type1, string type2);

    // Error when the expression is not a boolean where required
    void displayNotBooleanError(string statement);

    // Error when the actual params and formal params dont match
    void displayActualParamsError(string functionId, vector<string> formalParams, vector<string> actualParams);


    // Visit method for each different AST node
    virtual void visit(ASTBlockNode * blockNode) override;

    virtual void visit(ASTProgramNode * programNode) override;

    virtual void visit(ASTIdentifierNode * identifierNode) override ;

    virtual void visit(ASTWhileNode * whileNode) override;

    virtual void visit(ASTVariableDeclNode * variableDeclNode) override ;

    virtual void visit(ASTTermNode * termNode) override;

    virtual void visit(ASTSimpleExpressionNode * simpleExpressionNode) override ;

    virtual void visit(ASTExpressionNTNode * expressionNtNode) override ;

    virtual void visit(ASTLiteralNode * literalNode) override ;

    virtual void visit(ASTFunctionCallNode * functionCallNode) override ;

    virtual void visit(ASTActualParamsNode * actualParamsNode) override;

    virtual void visit(ASTAssignmentNode * assignmentNode) override ;

    virtual void visit(ASTPrintNode * printNode) override;

    virtual void visit(ASTReturnNode * returnNode) override;

    virtual void visit(ASTIfNode * ifNode) override;

    virtual void visit(ASTForNode * forNode) override;

    virtual void visit(ASTFormalParamNode * formalParamNode) override;

    virtual void visit(ASTFormalParamsNode * formalParamsNode) override;

    virtual void visit(ASTFunctionDeclNode * functionDeclNode) override;

    virtual void visit(ASTUnaryNode * unaryNode) override ;

};


#endif //COMPLIER_INTERPRETEREXECUTIONVISITOR_H
