//
// Created by attar on 06-May-20.
//

#ifndef COMPLIER_XMLGENERATIONVISITOR_H
#define COMPLIER_XMLGENERATIONVISITOR_H

#include "Visitor.h"
#include "iostream"
#include <fstream>
#include <iostream>

#include "../ASTNodes/ExpressionNodes/ASTIdentifierNode.h"
#include "../ASTNodes/ExpressionNodes/ASTExpressionNode.h"
#include "../ASTNodes/ExpressionNodes/ASTUnaryNode.h"
#include "../ASTNodes/ExpressionNodes/ASTSimpleExpressionNode.h"
#include "../ASTNodes/ExpressionNodes/ASTTermNode.h"
#include "../ASTNodes/ExpressionNodes/ASTLiteralNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFunctionCallNode.h"
#include "../ASTNodes/ExpressionNodes/ASTExpressionNTNode.h"
#include "../ASTNodes/ExpressionNodes/ASTActualParamsNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFormalParamNode.h"
#include "../ASTNodes/ExpressionNodes/ASTFormalParamsNode.h"

#include "../ASTNodes/StatementNodes/ASTAssignmentNode.h"
#include "../ASTNodes/StatementNodes/ASTPrintNode.h"
#include "../ASTNodes/StatementNodes/ASTReturnNode.h"
#include "../ASTNodes/StatementNodes/ASTProgramNode.h"
#include "../ASTNodes/StatementNodes/ASTReturnNode.h"
#include "../ASTNodes/StatementNodes/ASTIfNode.h"
#include "../ASTNodes/StatementNodes/ASTBlockNode.h"
#include "../ASTNodes/StatementNodes/ASTForNode.h"
#include "../ASTNodes/StatementNodes/ASTWhileNode.h"
#include "../ASTNodes/StatementNodes/ASTStatementNode.h"
#include "../ASTNodes/StatementNodes/ASTVariableDeclNode.h"
#include "../ASTNodes/StatementNodes/ASTFunctionDeclNode.h"


using namespace std;


class XMLGenerationVisitor :public Visitor {
private:
    // Stream to write to file
    ofstream * outfile;

    // Storing the indent pattern
    string indent;

    // Function to increase the indent
    void IncreaseIndent();

    // Function to decrease the indent
    void DecreaseIndent();

public:
    // Constructor
    XMLGenerationVisitor(ofstream * outfile);

    // Visit method for each different AST node
    virtual void visit(ASTBlockNode * blockNode) override;

    virtual void visit(ASTProgramNode * programNode) override;

    virtual void visit(ASTVariableDeclNode * variableDeclNode) override ;

    virtual void visit(ASTTermNode * termNode) override;

    virtual void visit(ASTSimpleExpressionNode * simpleExpressionNode) override ;

    virtual void visit(ASTExpressionNTNode * expressionNtNode) override ;

    virtual void visit(ASTLiteralNode * literalNode) override ;

    virtual void visit(ASTIdentifierNode * identifierNode) override ;

    virtual void visit(ASTFunctionCallNode * functionCallNode) override ;

    virtual void visit(ASTActualParamsNode * actualParamsNode) override;

    virtual void visit(ASTAssignmentNode * assignmentNode) override ;

    virtual void visit(ASTPrintNode * printNode) override;

    virtual void visit(ASTReturnNode * returnNode) override;

    virtual void visit(ASTIfNode * ifNode) override;

    virtual void visit(ASTForNode * forNode) override;

    virtual void visit(ASTWhileNode * whileNode) override;

    virtual void visit(ASTFormalParamNode * formalParamNode) override;

    virtual void visit(ASTFormalParamsNode * formalParamsNode) override;

    virtual void visit(ASTFunctionDeclNode * functionDeclNode) override;

    virtual void visit(ASTUnaryNode * unaryNode) override ;
};


#endif //COMPLIER_XMLGENERATIONVISITOR_H
