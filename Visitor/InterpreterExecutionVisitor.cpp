#include "InterpreterExecutionVisitor.h"


// Constructor
InterpreterExecutionVisitor::InterpreterExecutionVisitor() {
    // Storing the object of type SymbolTable in the class variable
    this->symbolTable = new SymbolTable();
}

void InterpreterExecutionVisitor::visit(ASTBlockNode *blockNode) {
    // Going through the list of statement nodes in the block node
    for (auto it = blockNode->statementNodeList.begin(); it != blockNode->statementNodeList.end(); ++it) {
        (*it)->accept(this);
    }
}

void InterpreterExecutionVisitor::visit(ASTProgramNode * programNode) {
    // Adding a new scope
    symbolTable->push();

    // Going through the list of statement nodes in the program node
    for (auto it = programNode->statementNodeList.begin(); it != programNode->statementNodeList.end(); ++it) {
        (*it)->accept(this);
    }

    // Removing the scope
    symbolTable->pop();
}

void InterpreterExecutionVisitor::visit(ASTIdentifierNode * identifierNode) {
    id = true;

    tuple<string,string,int> idLookUpResult;

    // Looking up the function or the variable in the bindings of the current scope
    // A tuple is returned containing the type of the identifier and the scope number it was found in
    if (currentFunctionDeclarationOrCall) {
        idLookUpResult  = symbolTable->lookupFunction(identifierNode->id);
    } else {
        idLookUpResult  = symbolTable->lookupVariable(identifierNode->id);
    }


    currentId = identifierNode->id;

    if(currentVariableDeclaration || currentFunctionDeclarationOrCall) {
        // If the identifier was not found in the bindings
        if (get<0>(idLookUpResult).compare("notFound") == 0) {
            idFound = false;
            idFoundInParentScope = false;
            currentType = identifierNode->type;

        } else if (get<2>(idLookUpResult) != symbolTable->scopes.size()) {
            // The id was not found in the last scope
            // Than the same id was used but in a different scope but it can be redeclared in this scope
            idFound = false;
            idFoundInParentScope = true;
            currentType = identifierNode->type;

        } else {
            // Identifier already defined in this scope
            idFound = true;
            idFoundInParentScope = false;
            currentType = get<0>(idLookUpResult);
            currentValue = get<1>(idLookUpResult);
        }
    } else {
        // If not a variable declaration or a function declaration or call
        // If the identifier was not found
        if (get<0>(idLookUpResult).compare("notFound") == 0) {
            idFound = false;
            idFoundInParentScope = false;
            currentType = identifierNode->type;

        } else {
            // If the identifier was found
            idFound = true;
            idFoundInParentScope = false;
            currentType = get<0>(idLookUpResult);
            currentValue = get<1>(idLookUpResult);

        }
    }


    // Resetting for next call
    currentVariableDeclaration = false;
    currentFunctionDeclarationOrCall = false;

}

void InterpreterExecutionVisitor::visit(ASTVariableDeclNode * variableDeclNode){
    currentVariableDeclaration = true;

    // Visiting the identifier node
    variableDeclNode->identifierNode->accept(this);
    // Storing the identifiers id and type locally
    string variableType  = currentType;
    string variableId = currentId;

    // If an identifier was found but not found in the binding it is added to the binding
    // If it was found in the binding an error message is returned
    if(id && !idFound) {
        // Adding the variable to the map of the current scope
        symbolTable->insertVariable(currentId, currentType);
    }

    // Visiting the expression node
    variableDeclNode->expressionNode->accept(this);

    // If the variable type was auto, the variable type is now set to the type deduced from the expression
    if (variableType.compare("auto") == 0) {
        symbolTable->scopes.top()->variableBindings[variableId] = make_tuple(currentType,get<1>(symbolTable->scopes.top()->variableBindings[variableId]));
        variableType = currentType;
    }

    // Adding the value to the identifier
    symbolTable->updateVariableValue(variableId,currentValue);
}

void InterpreterExecutionVisitor::visit(ASTTermNode * termNode){
    int counter=0;

    // Initializing an empty list of pairs
    string emptyString;
    vector<pair<string, string>> tempVectorOfString;

    // Adding a new pair of vector of strings and vector of strings
    expressionTypesValuesStack.push(tempVectorOfString);

    // Going through the list of factor nodes in the term node
    for (auto it = termNode->factorNodesList.begin(); it != termNode->factorNodesList.end(); ++it) {
        counter++;
        (*it)->accept(this);

        // Adding the type and value of the factor
        expressionTypesValuesStack.top().push_back(make_pair(currentType,currentValue));

        // Adding the multiplicative operators after factor (after the second factor)
        if (counter >= 2) {
            // Adding the operator
            // The first operator should be added after adding two operands and after every operand thereafter
            expressionTypesValuesStack.top().push_back(make_pair("op",termNode->multiplicativeOpList[counter - 2]));
        }
    }

    // Calculating the result and storing the result type and value
    auto calcReturn = postFixCalculator();

    currentType = calcReturn.first;
    currentValue = calcReturn.second;

    expressionTypesValuesStack.pop();
}

void InterpreterExecutionVisitor::visit(ASTSimpleExpressionNode * simpleExpressionNode){
    int counter=0;

    // Initializing an empty list of pairs
    string emptyString;
    vector<pair<string, string>> tempVectorOfString;

    // Adding a new pair of vector of strings and vector of strings
    expressionTypesValuesStack.push(tempVectorOfString);

    // Going through the list of term nodes in the simple expression node
    for (auto it = simpleExpressionNode->termNodesList.begin(); it != simpleExpressionNode->termNodesList.end(); ++it) {
        counter++;
        (*it)->accept(this);

        // Adding the type and value of the factor
        expressionTypesValuesStack.top().push_back(make_pair(currentType,currentValue));

        // Adding the additive operators after the term (after the second term)
        if (counter >= 2) {
            expressionTypesValuesStack.top().push_back(make_pair("op",simpleExpressionNode->additiveOpList[counter - 2]));
        }
    }

    // Calculating the result and storing the result type and value
    auto calcReturn = postFixCalculator();

    currentType = get<0>(calcReturn);
    currentValue = get<1>(calcReturn);


    expressionTypesValuesStack.pop();
}

void InterpreterExecutionVisitor::visit(ASTExpressionNTNode * expressionNtNode){
    int counter=0;

    // Initializing an empty list of pairs
    string emptyString;
    vector<pair<string, string>> tempVectorOfString;

    // Adding a new pair of vector of strings and vector of strings
    expressionTypesValuesStack.push(tempVectorOfString);

    // Going through the list of simple expression nodes in the expression nt node
    for (auto it = expressionNtNode->simpleExpressionNodesList.begin(); it != expressionNtNode->simpleExpressionNodesList.end(); ++it) {
        counter++;
        (*it)->accept(this);

        // Adding the type and value of the factor
        expressionTypesValuesStack.top().push_back(make_pair(currentType,currentValue));


        // Adding the relational operators after the simple expression (after the second simple expression)
        if (counter >= 2) {
            expressionTypesValuesStack.top().push_back(make_pair("op",expressionNtNode->relationalOpList[counter - 2]));
        }

    }

    // Calculating the result and storing the result type and value
    auto calcReturn = postFixCalculator();


    currentType = get<0>(calcReturn);
    currentValue = get<1>(calcReturn);


    expressionTypesValuesStack.pop();
}

void InterpreterExecutionVisitor::visit(ASTLiteralNode * literalNode){
    // Storing the type of the literal for type mismatch checking
    currentType = literalNode->type;

    // Storing the value of the literal
    currentValue = literalNode->value;

    // An identifier hasn't been found hence id = false
    id = false;
}

void InterpreterExecutionVisitor::visit(ASTFunctionCallNode * functionCallNode){
    currentFunctionDeclarationOrCall = true;
    currentFunctionCall.push(true);
    calledFromFunctionCall = true;

    // Visiting the identifier node
    functionCallNode->identifierNode->accept(this);

    // Storing the function id locally
    string orignalFunctionCallId = currentId;

    //Adding the function id to a stack
    currentFunctionId.push(currentId);

    ASTFunctionDeclNode * functionDeclNode = nullptr;
    try {
        //Obtaining the pointer to the function node with the current function id
        functionDeclNode = functionIdPointermap.at(currentFunctionId.top());
    } catch( out_of_range ) {
        // This should never be the case since the identifier was already confirmed to be present by the semantic analysis
        cout<<"Error when calling function, function declaration node not found" <<endl;
    }


    // Visiting the actual Params node
    functionCallNode->actualParamsNode->accept(this);

    // There are again made true here since the actual params could have another function call inside them
    currentFunctionDeclarationOrCall = true;
    calledFromFunctionCall = true;


    // Visiting the function declaration node itself
    functionDeclNode->accept(this);

}

void InterpreterExecutionVisitor::visit(ASTActualParamsNode * actualParamsNode){
    vector<string> actualParamsList;

    currentFunctionActualParamsValues.push(actualParamsList);

    // Searching for the function in the bindings
    tuple<string, string, int> lookUpReturn = symbolTable->lookupFunction(currentFunctionId.top());

    // Declaring a copy of the stack of scopes
    stack<Scope *> tempScopesStack = symbolTable->scopes;

    // Obtaining the scope which the binding was found in
    while(tempScopesStack.size() != get<2>(lookUpReturn)) {
        tempScopesStack.pop();
    }

    // Obtaining the value of the binding with the functionId as key
    // The value is a tuple of a string and list of string, representing the return type and formal parameters respectively
    auto tupleReturnTypeParameterTypes = tempScopesStack.top()->functionBindings[currentFunctionId.top()];


    // Going through the list of expression nodes in the expression actual params node
    for (auto it = actualParamsNode->expressionNodeList.begin(); it != actualParamsNode->expressionNodeList.end(); ++it) {
        (*it)->accept(this);

        // Adding the actual params values to a list
        currentFunctionActualParamsValues.top().push_back(currentValue);

        // Checking for no expression type mismatch
        if (currentType.compare("invalid") != 0) {

            // Adding the found parameter to a list
            actualParamsList.push_back(currentType);
        }


    }
}

void InterpreterExecutionVisitor::visit(ASTAssignmentNode * assignmentNode){

    // Visiting the identifier node
    assignmentNode->identifierNode->accept(this);
    string variableId = currentId;
    string identifierType = currentType;

    // Visiting the expression node
    assignmentNode->expressionNode->accept(this);

    // Adding the value to the identifier
    symbolTable->updateVariableValue(variableId,currentValue);

}

void InterpreterExecutionVisitor::visit(ASTPrintNode * printNode){

    // Visiting the expression node
    printNode->expressionNode->accept(this);

    //If a function declaration is not currently being executed, any print statement can be displayed
    if (currentFunctionDeclaration.size() == 0) {
        cout<< currentValue <<endl;
    }

}

void InterpreterExecutionVisitor::visit(ASTIfNode * ifNode){
    // Visiting the expression node
    ifNode->expressionNode->accept(this);

    // If the expression value is true, the block of statement in the true block is exectued
    if(currentValue.compare("true") == 0) {
        // Adding a new scope
        symbolTable->push();

        // Visiting the true block node
        ifNode->trueBlockNode->accept(this);

        // Removing the scope
        symbolTable->pop();
    } else if (currentValue.compare("false") == 0 && ifNode->falseBlockNode != nullptr) {
        // if the above expression results in false and a false block is available, this is executed
        // Adding a new scope
        symbolTable->push();

        // Since the else part is optional
        if(ifNode->falseBlockNode != nullptr) {
            // Visiting the true block node
            ifNode->falseBlockNode->accept(this);
        }


        // Removing the scope
        symbolTable->pop();
    }
}

void InterpreterExecutionVisitor::visit(ASTForNode * forNode){

    // This variable declaration can be done not in the for loop and hence a new scope is not pushed until the block node
    if ( forNode->variableDeclNode != nullptr) {
        // Visiting the variable declaration node
        forNode->variableDeclNode->accept(this);
    }

    // If the current operator is not a function declaration
    if(currentFunctionDeclaration.size() == 0 ||!currentFunctionDeclaration.top()){
        while (true) {

            // Visiting the expresion node
            forNode->expressionNode->accept(this);

            // If the expression results in false, the loop is stopped
            if (currentValue.compare("false") == 0) {
                break;
            }

            // Adding a new scope
            symbolTable->push();

            // Visiting the block node
            forNode->blockNode->accept(this);

            // Removing the scope
            symbolTable->pop();

            // Since assignment is optional it is checked not to be null
            if (forNode->assignmentNode != nullptr){
                // Visiting the assignment node
                forNode->assignmentNode->accept(this);
            }

        }
    }


}

void InterpreterExecutionVisitor::visit(ASTFormalParamNode * formalParamNode){
    // This is so if the same parameter identifier is used is the previous scope, it can still be used here
    currentVariableDeclaration = true;
    formalParamNode->identifierNode->accept(this);

    // Adding an identifier binding to the current scope so this variable can be used by the rest of the function
    if(id && !idFound) {
        if(currentFunctionCall.top() || currentFunctionDeclaration.size() != 0) {
            // Adding the variable to the map of the current scope
            symbolTable->insertVariable(currentId, currentType);

        }
    }

    // If the current operation is not a function call, add the formal parameter to the function defenition
    if(!currentFunctionCall.top()){
        // Adding the parameter type to the binding of the function
        symbolTable->addParameterToFunction(currentFunctionId.top(),currentType);
    }

}

void InterpreterExecutionVisitor::visit(ASTFormalParamsNode * formalParamsNode){
    int counter = 0;
    // Going through the formal param nodes in the expression formal params node
    for (auto it = formalParamsNode->formalParamNodesList.begin(); it != formalParamsNode->formalParamNodesList.end(); ++it) {
        (*it)->accept(this);

        // If the current operation is a function call, the formal params value is updated
        if(currentFunctionCall.top()){
            symbolTable->updateVariableValue(currentId,currentFunctionActualParamsValues.top().at(counter));
        }
        counter++;
    }

    if (currentFunctionActualParamsValues.size() != 0){
        currentFunctionActualParamsValues.top().clear();
        currentFunctionActualParamsValues.pop();
    }

}

void InterpreterExecutionVisitor::visit(ASTFunctionDeclNode * functionDeclNode){
    //If the previous operation was not a function call hence this is a function declaration
    if (!calledFromFunctionCall) {
        currentFunctionCall.push(false);
    } else if (calledFromFunctionCall) {
        // If the previous operation was a function call hence this is a function call
        calledFromFunctionCall = false;
    }

    // If the current operation is not a function call
    if (!currentFunctionCall.top()){
        // Inserting true in the function declaration stack, since the current operation is a function declaration
        currentFunctionDeclaration.push(true);
        currentFunctionDeclarationOrCall = true;

        // Visiting the identifier Node
        functionDeclNode->identifierNode->accept(this);
        // In this case this is the return type of the function
        //string identifierType = currentType;

        // If the variable identifier wasn'currentFunctionCall found, a new binding is added with the identifier and the function return type
        if(id && !idFound) {
            // Adding the variable to the map of the current scope
            symbolTable->insertFunction(currentId, functionDeclNode->type);
        }

        currentFunctionId.push(currentId);

        // Adding to a map the function id and pointer to the function decl node
        functionIdPointermap.insert({currentFunctionId.top(),functionDeclNode});

        // Adding a new scope
        symbolTable->push();

        // If the function has formal parameters these are visited
        if (functionDeclNode->formalParamsNode != nullptr) {
            // Visiting the formal params node
            functionDeclNode->formalParamsNode->accept(this);
        }

        // Visiting the block node
        functionDeclNode->blockNode->accept(this);

        // Removing the scope
        symbolTable->pop();

        // Removing the function declaration boolean from the stack
        if (currentFunctionDeclaration.size() != 0) {
            currentFunctionDeclaration.pop();
        }

    } else {
        // A function call operation in place
        // Adding a new scope
        symbolTable->push();

        // If the function has formal parameters these are visited
        if (functionDeclNode->formalParamsNode != nullptr) {
            // Visiting the formal params node
            functionDeclNode->formalParamsNode->accept(this);
        }



        // Visiting the block node
        functionDeclNode->blockNode->accept(this);

        // Removing the scope
        symbolTable->pop();

        //Removing the function call boolean
        if(currentFunctionCall.size() != 0) {
            currentFunctionCall.pop();
        }
    }

    currentFunctionId.pop();
}

void InterpreterExecutionVisitor::visit(ASTReturnNode * returnNode){
    // Visiting the expression node
    returnNode->expressionNode->accept(this);

    // Checking the return type of the function we are returning from
    auto lookUpReturn = symbolTable->lookupFunction(currentFunctionId.top());

    // Updating the function return value with the value of the expression
    symbolTable->updateFunctionValue(currentFunctionId.top(),currentValue);

    // Checking for no expression type mismatch
    if (currentType.compare("invalid") != 0) {
        // If the function has auto return type
        if (get<0>(lookUpReturn).compare("auto") == 0){
            // Changing the type of the function to the one derived from the above expression
            symbolTable->changeFunctionType(currentFunctionId.top(),currentType);

        }
    }
}

void InterpreterExecutionVisitor::visit(ASTUnaryNode * unaryNode){
    //Visiting expression node
    unaryNode->expressionNode->accept(this);

    //Checking that the expresion is of type boolean
    if(currentType.compare("bool") == 0) {
        // Inverting boolean value
        if (currentValue.compare("true") ==0) {
            currentValue = "false";
        } else if (currentValue.compare("false")==0) {
            currentValue = "true";
        }
    } else if (currentType.compare("int") == 0 || currentType.compare("float") == 0) {
        // If the value if an integer or float, a minus sign is added in front
        currentValue = "-" + currentValue;

    }
}

void InterpreterExecutionVisitor::visit(ASTWhileNode * whileNode) {
    // If the current operator is not a function declaration
    if(currentFunctionDeclaration.size() == 0 || !currentFunctionDeclaration.top()) {
        while (true) {
            // Visiting the expression node
            whileNode->expressionNode->accept(this);

            // If the boolean expression results in false the while loop is ended
            if(currentValue.compare("false") == 0) {
                break;
            }

            // Adding a new scope
            symbolTable->push();

            // Visiting the block node
            whileNode->blockNode->accept(this);

            // Removing a scope
            symbolTable->pop();
        }

    }

}

void InterpreterExecutionVisitor::displayErrorMessage(string id, bool duplicate) {
    // The error message if a variable was not declared or declared twice
    if(!semanticError) {
        semanticError = true;
        if (duplicate){
            cout << "Semantic Error: " << id << " has already been declared!"<< endl;
        } else {
            cout << "Semantic Error: " << id << " is not declared!"<< endl;
        }
    }

}

void InterpreterExecutionVisitor::displayTypeMismatchErrorMessage(string type1, string type2) {
    // Error for type mismatch between identifier and expression
    if(!semanticError){
        semanticError = true;
        cout << "Semantic Error | Type Mismatch: " << type1 << " != " << type2 << endl;
    }

}

pair<string,string> InterpreterExecutionVisitor::postFixCalculator() {
    // Stack of type and value
    stack<pair<string, string>> calcStack;

    if(expressionTypesValuesStack.top().size() == 1) {
        // Only 1 value, not calculation needed
        return expressionTypesValuesStack.top().front();
    } else {
        for (auto it = expressionTypesValuesStack.top().begin(); it != expressionTypesValuesStack.top().end(); ++it) {
            if(calculationError) {
                break;
            }
            if ((*it).first.compare("op") == 0) {

                // Value 2 is before value 1 since they are popped from a stack (LIFO)
                auto value2 = calcStack.top();
                calcStack.pop();

                int value2Int;
                float value2Float;
                bool value2Bool;

                if (currentFunctionDeclaration.size() == 0) {
                    if (value2.first.compare("int") == 0) {
                        value2Int = stoi(value2.second);
                    } else if(value2.first.compare("float") == 0) {
                        value2Float = stof(value2.second.c_str());
                    } else if(value2.first.compare("bool") == 0) {
                        if (value2.second.compare("true") == 0) {
                            value2Bool = true;
                        } else if (value2.second.compare("false") == 0){
                            value2Bool = false;
                        }
                    }
                }



                // Obtaining the last to expression values
                auto value1 = calcStack.top();
                calcStack.pop();

                int value1Int;
                float value1Float;
                bool value1Bool;

                if (currentFunctionDeclaration.size() == 0) {
                    if (value1.first.compare("int") == 0) {
                        value1Int = stoi(value1.second);
                    } else if(value1.first.compare("float") == 0) {
                        value1Float = stof(value1.second.c_str());
                    } else if(value1.first.compare("bool") == 0) {
                        if (value1.second.compare("true") == 0) {
                            value1Bool = true;
                        } else if (value1.second.compare("false") == 0){
                            value1Bool = false;
                        }
                    }
                }



                string resultValue = "";
                string resultType = "";

                if ((*it).second.compare("*") == 0) {
                    if (value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        resultType = "int";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Int * value2Int);
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Float * value2Int);
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Int * value2Float);
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Float * value2Float);
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("/") == 0) {
                    if (value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        // int divided by int results in 0
                        // casting to float solves this issue
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string((float) value1Int / (float) value2Int);
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Float / value2Int);
                    } else if (get<0>(value1).compare("int") == 0 && value2.first.compare("float") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Int / value2Float);
                    } else if (get<0>(value1).compare("float") == 0 && value2.first.compare("float") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Float / value2Float);
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("and") == 0) {
                    if (value1.first.compare("bool") == 0 && value2.first.compare("bool") == 0) {
                        resultType = "bool";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Bool && value2Bool);
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("+") == 0) {
                    if (value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        resultType = "int";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Int + value2Int);
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Float + value2Int);
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Int + value2Float);
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Float + value2Float);
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("-") == 0) {
                    if (value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        resultType = "int";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Int - value2Int);
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Float - value2Int);
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Int - value2Float);
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        resultType = "float";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Float - value2Float);
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("or") == 0) {
                    if (value1.first.compare("bool") == 0 && value2.first.compare("bool") == 0) {
                        resultType = "bool";
                        if(currentFunctionDeclaration.size() == 0) resultValue = to_string(value1Bool || value2Bool);
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("==") == 0) {
                    resultType = "bool";
                    if (value1.first.compare("bool") == 0 && value2.first.compare("bool") == 0) {
                        resultType = "bool";
                        if (value1Bool == value2Bool){
                            if(currentFunctionDeclaration.size() == 0) resultValue = "true";
                        } else if (value1Bool == !value2Bool){
                            if(currentFunctionDeclaration.size() == 0) resultValue = "false";
                        } else if (!value1Bool == value2Bool){
                            if(currentFunctionDeclaration.size() == 0) resultValue = "false";
                        } else if (!value1Bool == !value2Bool){
                            if(currentFunctionDeclaration.size() == 0) resultValue = "true";
                        } else {
                            calculationError = true;
                        }
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Int == value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }

                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float == value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }

                        }
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if((float) value1Int == value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }

                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float == (float) value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare(">") == 0) {
                    resultType = "bool";

                    if(value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Int > value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float > value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float > (float) value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if((float) value1Int > value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("<") == 0) {
                    resultType = "bool";

                    if(value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Int < value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float < value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float < (float) value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if((float) value1Int < value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("<>") == 0) {
                    resultType = "bool";
                    if (value1.first.compare("bool") == 0 && value2.first.compare("bool") == 0) {
                        resultType = "bool";
                        if (value1Bool != value2Bool){
                            if(currentFunctionDeclaration.size() == 0) resultValue = "false";
                        } else if (value1Bool != !value2Bool){
                            if(currentFunctionDeclaration.size() == 0) resultValue = "true";
                        } else if (!value1Bool != value2Bool){
                            if(currentFunctionDeclaration.size() == 0) resultValue = "true";
                        } else if (!value1Bool != !value2Bool){
                            if(currentFunctionDeclaration.size() == 0) resultValue = "false";
                        } else {
                            calculationError = true;
                        }
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Int != value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }

                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float != value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }

                        }
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if((float) value1Int != value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }

                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float != (float) value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare("<=") == 0) {
                    resultType = "bool";

                    if(value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Int <= value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float <= value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float <= (float) value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if((float) value1Int <= value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else {
                        calculationError = true;
                    }
                } else if ((*it).second.compare(">=") == 0) {
                    resultType = "bool";

                    if(value1.first.compare("int") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Int >= value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float >= value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("float") == 0 && value2.first.compare("int") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if(value1Float >= (float) value2Int){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else if (value1.first.compare("int") == 0 && value2.first.compare("float") == 0) {
                        if(currentFunctionDeclaration.size() == 0) {
                            if((float) value1Int >= value2Float){
                                resultValue = "true";
                            } else {
                                resultValue = "false";
                            }
                        }
                    } else {
                        calculationError = true;
                    }
                } else {
                    calculationError = true;
                }

                calcStack.push(make_pair(resultType,resultValue));
            } else {
                // Value found
                calcStack.push((*it));
            }
        }
    }

    if(calculationError) {
        cout<<"--CALCULATION ERROR--"<<endl;
    }

    return calcStack.top();
}

void InterpreterExecutionVisitor::displayNotBooleanError(string statement){
    // Error when the expression is not a boolean where required
    if (!semanticError) {
        semanticError = true;
        cout << "Expected Boolean expression in "<< statement <<" loop" << endl;
    }
}

void InterpreterExecutionVisitor::displayActualParamsError(string functionId, vector<string> formalParams, vector<string> actualParams){
    // Error when the actual params and formal params dont match
    if(!semanticError){
        semanticError = true;

        cout<< "Parameter mismatch when calling function " << functionId <<endl;
        cout<< "Expected Parameters: ";

        int counter = 0;
        // Displaying the expected parameters
        for (auto it = formalParams.begin(); it != formalParams.end(); ++it) {
            cout << (*it);

            if(counter != formalParams.size() - 1) {
                cout << " and ";
            }
            counter++;
        }


        if (counter == 0) {
            cout << "NONE" <<endl;
        } else  {
            cout<<""<<endl;
        }
        counter = 0;

        cout<< "Passed Parameters: ";

        // Displaying the actual parameters
        for (auto it = actualParams.begin(); it != actualParams.end(); ++it) {
            cout << (*it);

            if(counter != actualParams.size() - 1) {
                cout << " and ";
            }
            counter++;
        }

        if (counter == 0) {
            cout << "NONE" <<endl;
        } else  {
            cout<<""<<endl;
        }
    }
}